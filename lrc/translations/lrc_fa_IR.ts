<?xml version="1.0" ?><!DOCTYPE TS><TS language="fa_IR" sourcelanguage="en" version="2.0">
<context>
    <name>Account</name>
    <message>
        <location filename="../src/account.cpp" line="307"/>
        <source>Ready</source>
        <extracomment>Account state</extracomment>
        <translation>آماده</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="309"/>
        <source>Registered</source>
        <extracomment>Account state</extracomment>
        <translation>ثبت شده</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="313"/>
        <source>Initializing</source>
        <extracomment>Account state</extracomment>
        <translation>آغازِ فرایند</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="317"/>
        <source>Error</source>
        <extracomment>Account state</extracomment>
        <translation>خطا</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="321"/>
        <source>Network unreachable</source>
        <extracomment>Account state</extracomment>
        <translation>شبکه در دسترس نیست</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="323"/>
        <source>Host unreachable</source>
        <extracomment>Account state</extracomment>
        <translation>میزبان در دسترس نیست</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="311"/>
        <source>Not registered</source>
        <extracomment>Account state</extracomment>
        <translation>ثبت‌نشده</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="315"/>
        <source>Tryingâ¦</source>
        <extracomment>Account state</extracomment>
        <translation>درحال تلاشâ¦</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="319"/>
        <source>Authentication failed</source>
        <extracomment>Account state</extracomment>
        <translation>تأیید هویت شکست خورد</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="325"/>
        <source>STUN configuration error</source>
        <extracomment>Account state</extracomment>
        <translation>خطای پیکربندی STUN</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="327"/>
        <source>STUN server invalid</source>
        <extracomment>Account state</extracomment>
        <translation>کارساز نامعتبر STUN</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="329"/>
        <source>Service unavailable</source>
        <extracomment>Account state</extracomment>
        <translation>خدمات موجود نیست</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="331"/>
        <source>Unacceptable</source>
        <extracomment>Account state</extracomment>
        <translation>غیرقابل پذیرش</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="333"/>
        <source>Invalid</source>
        <extracomment>Account state</extracomment>
        <translation>نامعتبر</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="335"/>
        <source>Request timeout</source>
        <extracomment>Account state</extracomment>
        <translation>انقضای درخواست</translation>
    </message>
</context>
<context>
    <name>AccountChecksModel</name>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="438"/>
        <source>Configuration</source>
        <translation>پکربندی</translation>
    </message>
</context>
<context>
    <name>Call</name>
    <message>
        <location filename="../src/call.cpp" line="719"/>
        <source>New</source>
        <extracomment>Call state</extracomment>
        <translation>نو</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="722"/>
        <source>Ringing</source>
        <extracomment>Call state</extracomment>
        <translation>در حال زنگ خوردن</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="725"/>
        <source>Calling</source>
        <extracomment>Call state</extracomment>
        <translation>در حال تماس</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="728"/>
        <source>Talking</source>
        <extracomment>Call state</extracomment>
        <translation>در حال صحبت</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="731"/>
        <source>Dialing</source>
        <extracomment>Call state</extracomment>
        <translation>در حال شماره گیری</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="734"/>
        <source>Hold</source>
        <extracomment>Call state</extracomment>
        <translation>دست نگاه دارید</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="737"/>
        <source>Failed</source>
        <extracomment>Call state</extracomment>
        <translation>انجام نشد</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="740"/>
        <source>Busy</source>
        <extracomment>Call state</extracomment>
        <translation>مشغول</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="743"/>
        <source>Transfer</source>
        <extracomment>Call state</extracomment>
        <translation>انتقال</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="746"/>
        <source>Transfer hold</source>
        <extracomment>Call state</extracomment>
        <translation>انتقال انجام نشد</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="749"/>
        <source>Over</source>
        <extracomment>Call state</extracomment>
        <translation>تمام شد</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="752"/>
        <source>Error</source>
        <extracomment>Call state</extracomment>
        <translation>خطا</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="755"/>
        <location filename="../src/call.cpp" line="860"/>
        <source>Conference</source>
        <extracomment>Call state</extracomment>
        <translation>نشست</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="758"/>
        <source>Conference (hold)</source>
        <extracomment>Call state</extracomment>
        <translation>کنفرانس (در حالت تعلیق)</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="761"/>
        <source>ERROR</source>
        <extracomment>Call state</extracomment>
        <translation>خطا</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="764"/>
        <source>Searching for</source>
        <extracomment>Call state</extracomment>
        <translation>درحال جستجو برای</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="767"/>
        <source>Aborted</source>
        <extracomment>Call state</extracomment>
        <translation>لغو شد</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="770"/>
        <source>Communication established</source>
        <extracomment>Call state</extracomment>
        <translation>ارتباط برقرار شد</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="864"/>
        <source>Unknown</source>
        <translation>ناشناخته</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="2231"/>
        <source>Account: </source>
        <translation>حساب کاربری</translation>
    </message>
</context>
<context>
    <name>CallModel</name>
    <message>
        <location filename="../src/callmodel.cpp" line="898"/>
        <source>Calls</source>
        <translation>تماس‌ها</translation>
    </message>
</context>
<context>
    <name>CallModelPrivate</name>
    <message>
        <location filename="../src/callmodel.cpp" line="533"/>
        <location filename="../src/callmodel.cpp" line="558"/>
        <source>Invalid account</source>
        <translation>حساب کاربری نامعتبر</translation>
    </message>
</context>
<context>
    <name>CallPrivate</name>
    <message>
        <location filename="../src/call.cpp" line="1795"/>
        <source>Aborted</source>
        <translation>لغو شد</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="1816"/>
        <source>No account registered!</source>
        <translation>هیچ حسابی ثبت نشده است!</translation>
    </message>
</context>
<context>
    <name>CategorizedBookmarkModel</name>
    <message>
        <location filename="../src/categorizedbookmarkmodel.cpp" line="181"/>
        <source>Most popular</source>
        <extracomment>Most popular contacts</extracomment>
        <translation>محبوب‌ترین‌ها</translation>
    </message>
    <message>
        <location filename="../src/categorizedbookmarkmodel.cpp" line="299"/>
        <source>Contacts</source>
        <translation>لیست مخاطبین</translation>
    </message>
</context>
<context>
    <name>CategorizedContactModel</name>
    <message>
        <location filename="../src/categorizedcontactmodel.cpp" line="401"/>
        <source>Contacts</source>
        <translation>مخاطبین</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="129"/>
        <source>Empty</source>
        <translation>خالی</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="135"/>
        <location filename="../src/private/sortproxies.cpp" line="153"/>
        <source>Unknown</source>
        <translation>ناشناخته</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="141"/>
        <source>Never</source>
        <translation>هرگز</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="147"/>
        <source>Other</source>
        <translation>دیگر</translation>
    </message>
</context>
<context>
    <name>CategorizedHistoryModel</name>
    <message>
        <location filename="../src/categorizedhistorymodel.cpp" line="412"/>
        <source>History</source>
        <translation>تاریخچه</translation>
    </message>
</context>
<context>
    <name>ChainOfTrustModel</name>
    <message>
        <location filename="../src/chainoftrustmodel.cpp" line="173"/>
        <source>Chain of trust</source>
        <translation>ذنجیره اعتماد</translation>
    </message>
</context>
<context>
    <name>CollectionModel</name>
    <message>
        <location filename="../src/collectionmodel.cpp" line="279"/>
        <source>Name</source>
        <translation>نام</translation>
    </message>
</context>
<context>
    <name>ContactSortingCategoryModel</name>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="49"/>
        <source>Name</source>
        <translation>نام</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="50"/>
        <source>Organisation</source>
        <translation>سازمان</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="51"/>
        <source>Recently used</source>
        <translation>به تازگی استفاده شده</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="52"/>
        <source>Group</source>
        <translation>گروه</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="53"/>
        <source>Department</source>
        <translation>بخش</translation>
    </message>
</context>
<context>
    <name>HistorySortingCategoryModel</name>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="57"/>
        <source>Date</source>
        <translation>تاریخ</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="58"/>
        <source>Name</source>
        <translation>نام</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="59"/>
        <source>Popularity</source>
        <translation>محبوبیت</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="60"/>
        <source>Duration</source>
        <translation>مت زمان</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="61"/>
        <source>Total time</source>
        <translation>زمان کل</translation>
    </message>
</context>
<context>
    <name>HistoryTimeCategoryModel</name>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="50"/>
        <source>Today</source>
        <translation>امروز</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="51"/>
        <source>Yesterday</source>
        <translation>دیروز</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="58"/>
        <source>Two weeks ago</source>
        <translation>دو هفته پیش</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="59"/>
        <source>Three weeks ago</source>
        <translation>سه هفته پیش</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="57"/>
        <source>A week ago</source>
        <translation>هفته پیش</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="60"/>
        <source>A month ago</source>
        <translation>ماه پیش</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="61"/>
        <source>Two months ago</source>
        <translation>دو ماه پیش</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="62"/>
        <source>Three months ago</source>
        <translation>سه ماه پیش</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="63"/>
        <source>Four months ago</source>
        <translation>چهار ماه پیش</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="64"/>
        <source>Five months ago</source>
        <translation>پنج ماه پیش</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="65"/>
        <source>Six months ago</source>
        <translation>شش ماه پیش</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="66"/>
        <source>Seven months ago</source>
        <translation>هفت ماه پیش</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="67"/>
        <source>Eight months ago</source>
        <translation>هشت ماه پیش</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="68"/>
        <source>Nine months ago</source>
        <translation>نه ماه پیش</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="69"/>
        <source>Ten months ago</source>
        <translation>ده ماه پیش</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="70"/>
        <source>Eleven months ago</source>
        <translation>یازده ماه پیش</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="71"/>
        <source>Twelve months ago</source>
        <translation>دوازده ماه پیش</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="72"/>
        <source>A year ago</source>
        <translation>سال پیش</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="73"/>
        <source>Very long time ago</source>
        <translation>خیلی وقت پیش</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="74"/>
        <source>Never</source>
        <translation>هرگز</translation>
    </message>
</context>
<context>
    <name>InstantMessagingModel</name>
    <message>
        <location filename="../src/media/textrecording.cpp" line="816"/>
        <source>Me</source>
        <translation>من</translation>
    </message>
</context>
<context>
    <name>MacroModel</name>
    <message>
        <location filename="../src/macromodel.cpp" line="157"/>
        <source>Macros</source>
        <translation>ماکروها</translation>
    </message>
    <message>
        <location filename="../src/macromodel.cpp" line="263"/>
        <source>New</source>
        <translation>نو</translation>
    </message>
    <message>
        <location filename="../src/macromodel.cpp" line="264"/>
        <source>Other</source>
        <translation>دیگر</translation>
    </message>
</context>
<context>
    <name>MacroModelPrivate</name>
    <message>
        <location filename="../src/macromodel.cpp" line="77"/>
        <source>Other</source>
        <translation>دیگر</translation>
    </message>
</context>
<context>
    <name>NumberCategoryModel</name>
    <message>
        <location filename="../src/numbercategorymodel.cpp" line="56"/>
        <source>Uncategorized</source>
        <translation>دسته‌بندی نشده</translation>
    </message>
</context>
<context>
    <name>PersonModel</name>
    <message>
        <location filename="../src/personmodel.cpp" line="170"/>
        <source>Persons</source>
        <translation>افراد</translation>
    </message>
</context>
<context>
    <name>PhoneDirectoryModel</name>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="234"/>
        <source>This account does not support presence tracking</source>
        <translation>این حساب حضور را رد یابی نمیکند</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="237"/>
        <source>No associated account</source>
        <translation>بدون حساب مرتبط</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="305"/>
        <source>URI</source>
        <translation>تعیین کننده هویت منابع یکشکل یو آر ای</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="305"/>
        <source>Type</source>
        <translation>نوع</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="305"/>
        <source>Person</source>
        <translation>شخص</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="305"/>
        <source>Account</source>
        <translation>حساب کاربری</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="305"/>
        <source>State</source>
        <translation>وضعیت</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="305"/>
        <source>Call count</source>
        <translation>شمارش تماس ها</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="305"/>
        <source>Week count</source>
        <translation>شمارش هفته ها</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="306"/>
        <source>Trimester count</source>
        <translation>شمارش دوره سه ماهه</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="306"/>
        <source>Have Called</source>
        <translation>تماس گرفته اند</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="306"/>
        <source>Last used</source>
        <translation>آخرین استفاده</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="306"/>
        <source>Name_count</source>
        <translation>نام_شمارش</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="306"/>
        <source>Total (in seconds)</source>
        <translation>مجموع (به ثانیه)</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="306"/>
        <source>Popularity_index</source>
        <translation>محبوبیت_شاخص</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="307"/>
        <source>Bookmarked</source>
        <translation>نشانه گذاری شده</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="307"/>
        <source>Tracked</source>
        <translation>ردیابی شده</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="307"/>
        <source>Present</source>
        <translation>حاضر</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="307"/>
        <source>Presence message</source>
        <translation>پیام حضور</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="307"/>
        <source>UID</source>
        <translation>UID</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="307"/>
        <source>Has certificate</source>
        <translation>گواهینامه دارد</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="307"/>
        <source>Registered name</source>
        <translation>نام ثبت شده</translation>
    </message>
</context>
<context>
    <name>PresenceStatusModel</name>
    <message>
        <location filename="../src/presencestatusmodel.cpp" line="197"/>
        <source>Name</source>
        <translation>نام</translation>
    </message>
    <message>
        <location filename="../src/presencestatusmodel.cpp" line="197"/>
        <source>Message</source>
        <translation>پیام</translation>
    </message>
    <message>
        <location filename="../src/presencestatusmodel.cpp" line="197"/>
        <source>Color</source>
        <translation>رنگ</translation>
    </message>
    <message>
        <location filename="../src/presencestatusmodel.cpp" line="197"/>
        <source>Present</source>
        <translation>حاضر</translation>
    </message>
    <message>
        <location filename="../src/presencestatusmodel.cpp" line="197"/>
        <source>Default</source>
        <translation>پیش‌گزیده</translation>
    </message>
    <message>
        <location filename="../src/presencestatusmodel.cpp" line="308"/>
        <location filename="../src/presencestatusmodel.cpp" line="361"/>
        <source>Custom</source>
        <translation>سفارشی</translation>
    </message>
    <message>
        <location filename="../src/presencestatusmodel.cpp" line="308"/>
        <location filename="../src/presencestatusmodel.cpp" line="310"/>
        <location filename="../src/presencestatusmodel.cpp" line="354"/>
        <location filename="../src/presencestatusmodel.cpp" line="361"/>
        <source>N/A</source>
        <translation>قابل اجرا نیست</translation>
    </message>
</context>
<context>
    <name>ProfileModel</name>
    <message>
        <location filename="../src/profilemodel.cpp" line="637"/>
        <source>Profiles</source>
        <translation>پروفایل ها</translation>
    </message>
</context>
<context>
    <name>ProfileModelPrivate</name>
    <message>
        <location filename="../src/profilemodel.cpp" line="751"/>
        <source>New profile</source>
        <translation>پروفایل جدید</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/certificate.cpp" line="42"/>
        <source>Has a private key</source>
        <translation>یک کلید خصوصی دارد</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="43"/>
        <source>Is not expired</source>
        <translation>منقضی نشده است</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="44"/>
        <source>Has strong signing</source>
        <translation>امضای قوی دارد</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="45"/>
        <source>Is not self signed</source>
        <translation>خود نشانه نیست</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="46"/>
        <source>Have a matching key pair</source>
        <translation>یک جفت کلید تطبیق دارید.</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="47"/>
        <source>Has the right private key file permissions</source>
        <translation>دارای مجوز درست برای دسترسی به فایل خصوصی را دارد.</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="48"/>
        <source>Has the right public key file permissions</source>
        <translation>دارای مجوز درست برای دسترسی به فایل عمومی  را دارد.</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="49"/>
        <source>Has the right private key directory permissions</source>
        <translation>دارای مجوز درست برای دسترسی به دایرکتوری کلید خصوصی است.</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="50"/>
        <source>Has the right public key directory permissions</source>
        <translation>دارای مجوز درست برای دسترسی به دایرکتوری کلید عمومی است.</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="51"/>
        <source>Has the right private key directory location</source>
        <translation>مکان درست دایرکتوری کلید خصوصی را دارد.</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="52"/>
        <source>Has the right public key directory location</source>
        <translation>مکان درست دایرکتوری کلید عمومی را دارد.</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="53"/>
        <source>Has the right private key SELinux attributes</source>
        <translation>دارای کلید خصوصی درست برای دسترسی به SFLinux Attributes است.</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="54"/>
        <source>Has the right public key SELinux attributes</source>
        <translation>دارای کلید عمومی درست برای دسترسی به SFLinux Attributes است.</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="55"/>
        <source>The certificate file exist and is readable</source>
        <translation>فایل گواهی وجود دارد و قابل خواندن است.</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="56"/>
        <source>The file is a valid certificate</source>
        <translation>فایل موجود یک گواهی معتبر است.</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="57"/>
        <source>The certificate has a valid authority</source>
        <translation>این گواهی دارای مجوز معتبر است.</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="58"/>
        <source>The certificate has a known authority</source>
        <translation>گواهی دارای مجوز شناخته شده است.</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="59"/>
        <source>The certificate is not revoked</source>
        <translation>گواهی آن لغونشده است.</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="60"/>
        <source>The certificate authority match</source>
        <translation>گواهینامه مطابقت دارد.</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="61"/>
        <source>The certificate has the expected owner</source>
        <translation>گواهی متعلق به صاحب مورد نظر است.</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="62"/>
        <source>The certificate is within its active period</source>
        <translation>گواهی هنوز معتبر است.</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="90"/>
        <source>Expiration date</source>
        <translation>تاریخ انقضاء</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="91"/>
        <source>Activation date</source>
        <translation>تاریخ فعال سازی</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="92"/>
        <source>Require a private key password</source>
        <translation>نیاز به رمز عبور برای کلید خصوصی است.</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="93"/>
        <source>Public signature</source>
        <translation>امضای عمومی</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="94"/>
        <source>Version</source>
        <translation>نسخه</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="95"/>
        <source>Serial number</source>
        <translation>شماره سریال</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="96"/>
        <source>Issuer</source>
        <translation>صادر کننده</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="97"/>
        <source>Subject key algorithm</source>
        <translation>الگوریتم کلیدی موضوع</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="98"/>
        <source>Common name (CN)</source>
        <translation>نام مشترک (سی ان)</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="99"/>
        <source>Name (N)</source>
        <translation>نام (N)</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="100"/>
        <source>Organization (O)</source>
        <translation>سازمان (او)</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="101"/>
        <source>Signature algorithm</source>
        <translation>الگوریتم امضا</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="102"/>
        <source>Md5 fingerprint</source>
        <translation>اثرانگشت Md5</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="103"/>
        <source>Sha1 fingerprint</source>
        <translation>اثر انگشت Sha1</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="104"/>
        <source>Public key ID</source>
        <translation>شناسه کلید عمومی</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="105"/>
        <source>Issuer domain name</source>
        <translation>نام صادر کننده دومین</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="106"/>
        <source>Next expected update</source>
        <translation>به روز رسانی بعدی</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="107"/>
        <source>Outgoing server</source>
        <translation>سرور خروجی</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="183"/>
        <source>Local certificate store</source>
        <translation>فروشگاه گواهی محلی</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="267"/>
        <location filename="../src/localprofilecollection.cpp" line="220"/>
        <source>Default</source>
        <translation>پیش‌گزیده</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="267"/>
        <source>Certificate not associated with a group</source>
        <translation>گواهی با هیچ گروهی مرتبط نیست</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="397"/>
        <source>A certificate</source>
        <translation>یک گواهی</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="397"/>
        <source>An organisation</source>
        <translation>یک سازمان</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="420"/>
        <source>Details</source>
        <translation>جزییات</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="420"/>
        <source>The content of the certificate</source>
        <translation>محتوای گواهی</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="421"/>
        <source>Checks</source>
        <translation>بررسی ها</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="421"/>
        <source>Various security related information</source>
        <translation>اطلاعات مختلف مربوط به امنیت</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="610"/>
        <source>Header</source>
        <translation>سرتیتر</translation>
    </message>
    <message>
        <location filename="../src/daemoncertificatecollection.cpp" line="148"/>
        <source>Daemon certificate store</source>
        <translation>فروشگاه گواهی Daemon</translation>
    </message>
    <message>
        <location filename="../src/daemoncertificatecollection.cpp" line="152"/>
        <source>%1 banned list</source>
        <extracomment>The list of banned certificates for this account</extracomment>
        <translation>٪ 1 لیست ممنوع</translation>
    </message>
    <message>
        <location filename="../src/daemoncertificatecollection.cpp" line="155"/>
        <source>%1 allowed list</source>
        <extracomment>The list of allowed certificates for this account</extracomment>
        <translation>٪ 1  لیست مجاز</translation>
    </message>
    <message>
        <location filename="../src/daemoncertificatecollection.cpp" line="160"/>
        <location filename="../src/foldercertificatecollection.cpp" line="198"/>
        <source>Certificate</source>
        <translation>گواهی</translation>
    </message>
    <message>
        <location filename="../src/extensions/presencecollectionextension.cpp" line="38"/>
        <source>Presence tracking</source>
        <translation>ردیابی حضور</translation>
    </message>
    <message>
        <location filename="../src/extensions/securityevaluationextension.cpp" line="63"/>
        <source>Security evaluation</source>
        <translation>ارزیابی امنیت</translation>
    </message>
    <message>
        <location filename="../src/fallbackpersoncollection.cpp" line="194"/>
        <source>Contact</source>
        <translation>لطفا تماس بگیرید</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="532"/>
        <source>Bookmark</source>
        <translation>بوک مارک ها</translation>
    </message>
    <message>
        <location filename="../src/localhistorycollection.cpp" line="195"/>
        <source>Local history</source>
        <translation>تاریخ محلی</translation>
    </message>
    <message>
        <location filename="../src/localhistorycollection.cpp" line="200"/>
        <source>History</source>
        <translation>تاریخچه</translation>
    </message>
    <message>
        <location filename="../src/localrecordingcollection.cpp" line="101"/>
        <source>Local recordings</source>
        <translation>ضبط محلی</translation>
    </message>
    <message>
        <location filename="../src/localrecordingcollection.cpp" line="106"/>
        <location filename="../src/localtextrecordingcollection.cpp" line="170"/>
        <source>Recording</source>
        <translation>در حال ضبط</translation>
    </message>
    <message>
        <location filename="../src/localringtonecollection.cpp" line="220"/>
        <source>Local ringtones</source>
        <translation>آهنگ های زنگ محلی</translation>
    </message>
    <message>
        <location filename="../src/localringtonecollection.cpp" line="225"/>
        <source>Ringtone</source>
        <translation>آهنگ های زنگ</translation>
    </message>
    <message>
        <location filename="../src/localtextrecordingcollection.cpp" line="165"/>
        <source>Local text recordings</source>
        <translation> متون محلی ضبط شده</translation>
    </message>
    <message>
        <location filename="../src/numbercategory.cpp" line="72"/>
        <source>Phone number types</source>
        <translation>انواع شماره تلفن</translation>
    </message>
    <message>
        <location filename="../src/numbercategorymodel.cpp" line="186"/>
        <source>Other</source>
        <translation>دیگر</translation>
    </message>
    <message>
        <location filename="../src/protocolmodel.cpp" line="50"/>
        <source>Ring Account</source>
        <translation>حساب کاربری رینگ</translation>
    </message>
    <message>
        <location filename="../src/protocolmodel.cpp" line="51"/>
        <source>SIP Account</source>
        <translation>حساب کاربری سیپ</translation>
    </message>
    <message>
        <location filename="../src/qtwrapper/callmanager_wrap.h" line="315"/>
        <source>Me</source>
        <translation>من</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="38"/>
        <source>TLS is disabled, the negotiation won&apos;t be encrypted. Your communication will be vulnerable to snooping</source>
        <translation>TLS غیرفعال است، مذاکره رمزگذاری نخواهد شد. ارتباط شما برای سوء استفاده یا شنود آسیب پذیر خواهد بود خواهد بود.</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="40"/>
        <source>Your certificate and authority don&apos;t match, if your certificate require an authority, it won&apos;t work</source>
        <translation>گواهی و مجوز شما مطابقت ندارد، اگر گواهینامه شما مستلزم داشتن مجوز باشد، کار نمی کند.</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="41"/>
        <source>The outgoing server specified doesn&apos;t match the hostname or the one included in the certificate</source>
        <translation>سرور فرستاده شده مشخص شده با نام میزبان یا نام موجود در گواهی منطبق نیست.</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="42"/>
        <source>The &quot;verify incoming certificate&quot; option is disabled, this leave you vulnerable to man in the middle attack</source>
        <translation>گزینه &quot;تأیید گواهی ورودی&quot; غیرفعال است، این شما را آسیب پذیر می کند به مورد &quot;حمله به مردی که وسط است&quot;.</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="43"/>
        <source>The &quot;verify answer certificate&quot; option is disabled, this leave you vulnerable to man in the middle attack</source>
        <translation>گزینه « تأیید گواهی پاسخ» غیرفعال است، این شما را آسیب پذیر می کند به مورد &quot;حمله به مردی که وسط است&quot;.</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="44"/>
        <source>None of your certificate provide a private key, this is required. Please select a private key or use a certificate with one built-in</source>
        <translation>هیچکدام از گواهینامه شما یک کلید خصوصی ندارند، این مورد نیاز است. لطفا یک کلید خصوصی را انتخاب کنید، یا لطفا از یک گواهی استفاده کنید که دارای کلید خصوصی داخلی است.</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="47"/>
        <source>No certificate authority is provided, it won&apos;t be possible to validate if the answer certificates are valid. Some account may also not work.</source>
        <translation>هیچ گواهی اختیاری ارائه نشده است. اگر گواهی پاسخ معتبر باشد، امکان تایید آن وجود نخواهد داشت. برخی از حساب ها ممکن است کار نکنند.</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="46"/>
        <source>No certificate has been provided. This is, for now, unsupported by Ring</source>
        <translation>هیچ گواهی ارائه نشده است. این در حال حاضر، توسط رینگ پشتیبانی نمی شود.</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="37"/>
        <source>Your media streams are not encrypted, please enable SDES</source>
        <translation>جریان های رسانه ای شما رمزگذاری نمی شوند، لطفا SDES را فعال کنید.</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="50"/>
        <source>Your certificate is expired, please contact your system administrator.</source>
        <translation>گواهی شما منقضی شده است، لطفا با مدیر سیستم خود تماس بگیرید.</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="51"/>
        <source>Your certificate is self signed. This break the chain of trust.</source>
        <translation>گواهی شما خود امضا شده است این شکستن زنجیره اعتماد است.</translation>
    </message>
    <message>
        <location filename="../src/tlsmethodmodel.cpp" line="64"/>
        <source>Default</source>
        <comment>Default TLS protocol version</comment>
        <translation>پیش‌گزیده</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="517"/>
        <location filename="../src/useractionmodel.cpp" line="759"/>
        <source>Accept</source>
        <translation>قبول</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="60"/>
        <location filename="../src/useractionmodel.cpp" line="518"/>
        <location filename="../src/useractionmodel.cpp" line="771"/>
        <source>Hold</source>
        <translation>نگه داشتن</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="62"/>
        <source>Talking</source>
        <translation>در حال صحبت</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="64"/>
        <source>ERROR</source>
        <translation>خطا</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="66"/>
        <source>Incoming</source>
        <translation>تماس ورودی</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="68"/>
        <source>Calling</source>
        <translation>در حال تماس</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="70"/>
        <source>Connecting</source>
        <translation>در حال اتّصال</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="72"/>
        <source>Searching</source>
        <translation>در حال  جستجو</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="74"/>
        <source>Inactive</source>
        <translation>غیر فعال</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="76"/>
        <location filename="../src/api/call.h" line="82"/>
        <source>Finished</source>
        <translation>پایان یافته</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="78"/>
        <source>Timeout</source>
        <translation>انقضا</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="80"/>
        <source>Peer busy</source>
        <translation>همتا مشغول</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="84"/>
        <source>Communication established</source>
        <translation>ارتباط برقرار شد.</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="519"/>
        <source>Mute audio</source>
        <translation>قطع صدا</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="520"/>
        <source>Mute video</source>
        <translation>قطع ویدیو</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="521"/>
        <source>Server transfer</source>
        <translation>انتقال سرور</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="522"/>
        <source>Record</source>
        <translation>ضبط</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="523"/>
        <location filename="../src/useractionmodel.cpp" line="789"/>
        <source>Hangup</source>
        <translation>به صحبت تلفنی خاتمه دادن</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="524"/>
        <source>Join</source>
        <translation>پیوستن</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="525"/>
        <source>Add new</source>
        <translation>مورد جدیدی اضافه کنید.</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="526"/>
        <source>Toggle video</source>
        <translation>تعویض ویدیو</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="527"/>
        <source>Add a contact</source>
        <translation>یک مخاطب اضافه کنید</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="528"/>
        <source>Add to existing contact</source>
        <translation>به یک مخاطب موجود اضافه کنید</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="529"/>
        <source>Delete contact</source>
        <translation>مخاطب را حذف کنید</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="530"/>
        <source>Email contact</source>
        <translation>تماس با ایمیل</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="531"/>
        <source>Copy contact</source>
        <translation>کپی مخاطب</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="534"/>
        <source>Add phone number</source>
        <translation>شماره تلفن را اضافه کنید</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="535"/>
        <source>Call again</source>
        <translation>دوباره تماس بگیر</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="536"/>
        <source>Edit contact details</source>
        <translation>ویرایش جزئیات تماس</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="537"/>
        <source>Remove from history</source>
        <translation>حذف ازتاریخچه</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="786"/>
        <source>Remove</source>
        <translation>حذف کن</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="756"/>
        <source>Call</source>
        <translation>تماس</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="533"/>
        <source>Open chat</source>
        <translation>صفحهٔ مکالمه/ گپ را باز کنید</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="768"/>
        <source>Unhold</source>
        <translation>از سر گرفتن</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="779"/>
        <source>Cancel</source>
        <translation>لغو</translation>
    </message>
    <message>
        <location filename="../src/localprofilecollection.cpp" line="147"/>
        <source>Local profiles</source>
        <translation>پروفایل های محلی</translation>
    </message>
    <message>
        <location filename="../src/localprofilecollection.cpp" line="152"/>
        <source>Profile Collection</source>
        <translation>مجموعه مشخصات</translation>
    </message>
    <message>
        <location filename="../src/peerprofilecollection.cpp" line="141"/>
        <source>Peer profiles</source>
        <translation>پروفایل های همکار</translation>
    </message>
    <message>
        <location filename="../src/peerprofilecollection.cpp" line="146"/>
        <source>Peers Profiles Collection</source>
        <translation>مجموعه پروفیل های همکار</translation>
    </message>
    <message>
        <location filename="../src/conversationmodel.cpp" line="1265"/>
        <location filename="../src/conversationmodel.cpp" line="1649"/>
        <location filename="../src/conversationmodel.cpp" line="1909"/>
        <source>Invitation received</source>
        <translation>دعوت نامه دریافت شد</translation>
    </message>
    <message>
        <location filename="../src/conversationmodel.cpp" line="1266"/>
        <source>Contact added</source>
        <translation>مخاطب افزوده شد</translation>
    </message>
    <message>
        <location filename="../src/conversationmodel.cpp" line="1308"/>
        <location filename="../src/conversationmodel.cpp" line="1315"/>
        <source>Invitation accepted</source>
        <translation>دعوت پذیرفته شد</translation>
    </message>
    <message>
        <location filename="../src/conversationmodel.cpp" line="1524"/>
        <source>ð Outgoing call</source>
        <translation>ð تماس خروجی</translation>
    </message>
    <message>
        <location filename="../src/conversationmodel.cpp" line="1526"/>
        <source>ð Incoming call</source>
        <translation>ð تماس ورودی</translation>
    </message>
    <message>
        <location filename="../src/conversationmodel.cpp" line="1539"/>
        <source>ð Outgoing call - </source>
        <translation>ð تماس خروجی -</translation>
    </message>
    <message>
        <location filename="../src/conversationmodel.cpp" line="1542"/>
        <source>ð Incoming call - </source>
        <translation>ð تماس ورودی -</translation>
    </message>
    <message>
        <location filename="../src/conversationmodel.cpp" line="1546"/>
        <source>ð½ Missed outgoing call</source>
        <translation>ð½  تماس تلفنی رد شد</translation>
    </message>
    <message>
        <location filename="../src/conversationmodel.cpp" line="1548"/>
        <source>ð½ Missed incoming call</source>
        <translation>ð½ تماس تلفنی رد شد</translation>
    </message>
</context>
<context>
    <name>RingDeviceModel</name>
    <message>
        <location filename="../src/ringdevicemodel.cpp" line="110"/>
        <source>ID</source>
        <translation>شناسه</translation>
    </message>
    <message>
        <location filename="../src/ringdevicemodel.cpp" line="112"/>
        <source>Name</source>
        <translation>نام</translation>
    </message>
</context>
<context>
    <name>SecurityEvaluationModelPrivate</name>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="683"/>
        <source>Authority</source>
        <translation>اختیار</translation>
    </message>
</context>
<context>
    <name>TlsMethodModel</name>
    <message>
        <location filename="../src/tlsmethodmodel.cpp" line="101"/>
        <source>Automatic</source>
        <translation>خودکار</translation>
    </message>
</context>
<context>
    <name>Video::SourceModel</name>
    <message>
        <location filename="../src/video/sourcemodel.cpp" line="95"/>
        <source>NONE</source>
        <translation>هیچ کدام</translation>
    </message>
    <message>
        <location filename="../src/video/sourcemodel.cpp" line="101"/>
        <source>SCREEN</source>
        <translation>صفحه نمایش</translation>
    </message>
    <message>
        <location filename="../src/video/sourcemodel.cpp" line="107"/>
        <source>FILE</source>
        <translation>فایل</translation>
    </message>
</context>
<context>
    <name>lrc::ContactModelPimpl</name>
    <message>
        <location filename="../src/contactmodel.cpp" line="393"/>
        <source>Searchingâ¦</source>
        <translation>درحال جست‌وجوâ¦</translation>
    </message>
    <message>
        <location filename="../src/contactmodel.cpp" line="747"/>
        <source>Invalid ID</source>
        <translation>شناسهٔ نامعتبر</translation>
    </message>
    <message>
        <location filename="../src/contactmodel.cpp" line="750"/>
        <source>Not found</source>
        <translation>یافت نشد</translation>
    </message>
    <message>
        <location filename="../src/contactmodel.cpp" line="753"/>
        <source>Couldn&apos;t lookupâ¦</source>
        <translation>نمی‌توان جستâ¦</translation>
    </message>
</context>
<context>
    <name>lrc::api::ContactModel</name>
    <message>
        <location filename="../src/contactmodel.cpp" line="364"/>
        <source>Bad URI scheme</source>
        <translation>شمای نشانی بد</translation>
    </message>
</context>
<context>
    <name>media::RecordingModel</name>
    <message>
        <location filename="../src/media/recordingmodel.cpp" line="189"/>
        <source>Recordings</source>
        <translation>ضبط شده‌ها</translation>
    </message>
    <message>
        <location filename="../src/media/recordingmodel.cpp" line="259"/>
        <source>Text messages</source>
        <translation>پیام‌های متنی</translation>
    </message>
    <message>
        <location filename="../src/media/recordingmodel.cpp" line="264"/>
        <source>Audio/Video</source>
        <translation>صدا/تصویر</translation>
    </message>
</context>
</TS>