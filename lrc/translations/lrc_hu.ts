<?xml version="1.0" ?><!DOCTYPE TS><TS language="hu" sourcelanguage="en" version="2.0">
<context>
    <name>Account</name>
    <message>
        <location filename="../src/account.cpp" line="307"/>
        <source>Ready</source>
        <extracomment>Account state</extracomment>
        <translation>Készen áll</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="309"/>
        <source>Registered</source>
        <extracomment>Account state</extracomment>
        <translation>Regisztrált</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="313"/>
        <source>Initializing</source>
        <extracomment>Account state</extracomment>
        <translation>Szignál…</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="317"/>
        <source>Error</source>
        <extracomment>Account state</extracomment>
        <translation>Hiba</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="321"/>
        <source>Network unreachable</source>
        <extracomment>Account state</extracomment>
        <translation>A hálózat elérhetetlen</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="323"/>
        <source>Host unreachable</source>
        <extracomment>Account state</extracomment>
        <translation>A fogadó elérhetetlen</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="311"/>
        <source>Not registered</source>
        <extracomment>Account state</extracomment>
        <translation>Nem regisztrált</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="315"/>
        <source>Tryingâ¦</source>
        <extracomment>Account state</extracomment>
        <translation>Próbálâ¦</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="319"/>
        <source>Authentication failed</source>
        <extracomment>Account state</extracomment>
        <translation>Hitelesítési hiba</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="325"/>
        <source>STUN configuration error</source>
        <extracomment>Account state</extracomment>
        <translation>STUN beállítás hiba</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="327"/>
        <source>STUN server invalid</source>
        <extracomment>Account state</extracomment>
        <translation>STUN kiszolgáló érvénytelen</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="329"/>
        <source>Service unavailable</source>
        <extracomment>Account state</extracomment>
        <translation>A szolgáltatás nem elérhető</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="331"/>
        <source>Unacceptable</source>
        <extracomment>Account state</extracomment>
        <translation>Elfogadhatatlan</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="333"/>
        <source>Invalid</source>
        <extracomment>Account state</extracomment>
        <translation>Érvénytelen</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="335"/>
        <source>Request timeout</source>
        <extracomment>Account state</extracomment>
        <translation>Kérés időtúllépés</translation>
    </message>
</context>
<context>
    <name>AccountChecksModel</name>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="438"/>
        <source>Configuration</source>
        <translation>Beállítások</translation>
    </message>
</context>
<context>
    <name>Call</name>
    <message>
        <location filename="../src/call.cpp" line="719"/>
        <source>New</source>
        <extracomment>Call state</extracomment>
        <translation>Új</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="722"/>
        <source>Ringing</source>
        <extracomment>Call state</extracomment>
        <translation>Csörögni</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="725"/>
        <source>Calling</source>
        <extracomment>Call state</extracomment>
        <translation>Hívás</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="728"/>
        <source>Talking</source>
        <extracomment>Call state</extracomment>
        <translation>Hívásban</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="731"/>
        <source>Dialing</source>
        <extracomment>Call state</extracomment>
        <translation>Tárcsázás</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="734"/>
        <source>Hold</source>
        <extracomment>Call state</extracomment>
        <translation>Tartás</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="737"/>
        <source>Failed</source>
        <extracomment>Call state</extracomment>
        <translation>Sikertelen</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="740"/>
        <source>Busy</source>
        <extracomment>Call state</extracomment>
        <translation>Foglalt</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="743"/>
        <source>Transfer</source>
        <extracomment>Call state</extracomment>
        <translation>Átvitel</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="746"/>
        <source>Transfer hold</source>
        <extracomment>Call state</extracomment>
        <translation>Átvitel tartása</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="749"/>
        <source>Over</source>
        <extracomment>Call state</extracomment>
        <translation>Véget ért</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="752"/>
        <source>Error</source>
        <extracomment>Call state</extracomment>
        <translation>Hiba</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="755"/>
        <location filename="../src/call.cpp" line="860"/>
        <source>Conference</source>
        <extracomment>Call state</extracomment>
        <translation>Konferencia</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="758"/>
        <source>Conference (hold)</source>
        <extracomment>Call state</extracomment>
        <translation>Konferencia (tartás)</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="761"/>
        <source>ERROR</source>
        <extracomment>Call state</extracomment>
        <translation>HIBA</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="764"/>
        <source>Searching for</source>
        <extracomment>Call state</extracomment>
        <translation>Keresés</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="767"/>
        <source>Aborted</source>
        <extracomment>Call state</extracomment>
        <translation>Megszakítva</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="770"/>
        <source>Communication established</source>
        <extracomment>Call state</extracomment>
        <translation>A közlés létrejött</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="864"/>
        <source>Unknown</source>
        <translation>Ismeretlen</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="2231"/>
        <source>Account: </source>
        <translation>Fiók:</translation>
    </message>
</context>
<context>
    <name>CallModel</name>
    <message>
        <location filename="../src/callmodel.cpp" line="898"/>
        <source>Calls</source>
        <translation>Hívások</translation>
    </message>
</context>
<context>
    <name>CallModelPrivate</name>
    <message>
        <location filename="../src/callmodel.cpp" line="533"/>
        <location filename="../src/callmodel.cpp" line="558"/>
        <source>Invalid account</source>
        <translation>Érvénytelen fiók</translation>
    </message>
</context>
<context>
    <name>CallPrivate</name>
    <message>
        <location filename="../src/call.cpp" line="1795"/>
        <source>Aborted</source>
        <translation>Megszakítva</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="1816"/>
        <source>No account registered!</source>
        <translation>Nem regisztrált fiók!</translation>
    </message>
</context>
<context>
    <name>CategorizedBookmarkModel</name>
    <message>
        <location filename="../src/categorizedbookmarkmodel.cpp" line="181"/>
        <source>Most popular</source>
        <extracomment>Most popular contacts</extracomment>
        <translation>Legnépszerűbb</translation>
    </message>
    <message>
        <location filename="../src/categorizedbookmarkmodel.cpp" line="299"/>
        <source>Contacts</source>
        <translation>Partnerek</translation>
    </message>
</context>
<context>
    <name>CategorizedContactModel</name>
    <message>
        <location filename="../src/categorizedcontactmodel.cpp" line="401"/>
        <source>Contacts</source>
        <translation>Partnerek</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="129"/>
        <source>Empty</source>
        <translation>Üres</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="135"/>
        <location filename="../src/private/sortproxies.cpp" line="153"/>
        <source>Unknown</source>
        <translation>Ismeretlen</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="141"/>
        <source>Never</source>
        <translation>Soha</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="147"/>
        <source>Other</source>
        <translation>Másik</translation>
    </message>
</context>
<context>
    <name>CategorizedHistoryModel</name>
    <message>
        <location filename="../src/categorizedhistorymodel.cpp" line="412"/>
        <source>History</source>
        <translation>Előzmények</translation>
    </message>
</context>
<context>
    <name>ChainOfTrustModel</name>
    <message>
        <location filename="../src/chainoftrustmodel.cpp" line="173"/>
        <source>Chain of trust</source>
        <translation>A bizalom láncolata</translation>
    </message>
</context>
<context>
    <name>CollectionModel</name>
    <message>
        <location filename="../src/collectionmodel.cpp" line="279"/>
        <source>Name</source>
        <translation>Név</translation>
    </message>
</context>
<context>
    <name>ContactSortingCategoryModel</name>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="49"/>
        <source>Name</source>
        <translation>Név</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="50"/>
        <source>Organisation</source>
        <translation>Szervezet</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="51"/>
        <source>Recently used</source>
        <translation>Jelenleg használt</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="52"/>
        <source>Group</source>
        <translation>Csoport</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="53"/>
        <source>Department</source>
        <translation>Részleg</translation>
    </message>
</context>
<context>
    <name>HistorySortingCategoryModel</name>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="57"/>
        <source>Date</source>
        <translation>Dátum</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="58"/>
        <source>Name</source>
        <translation>Név</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="59"/>
        <source>Popularity</source>
        <translation>Népszerűség</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="60"/>
        <source>Duration</source>
        <translation>Időtartam</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="61"/>
        <source>Total time</source>
        <translation>Teljes idő</translation>
    </message>
</context>
<context>
    <name>HistoryTimeCategoryModel</name>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="50"/>
        <source>Today</source>
        <translation>Ma</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="51"/>
        <source>Yesterday</source>
        <translation>Tegnap</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="58"/>
        <source>Two weeks ago</source>
        <translation>Két héttel ezelőtt</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="59"/>
        <source>Three weeks ago</source>
        <translation>Három héttel ezelőtt</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="57"/>
        <source>A week ago</source>
        <translation>Egy héttel ezelőtt</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="60"/>
        <source>A month ago</source>
        <translation>Egy hónappal ezelőtt</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="61"/>
        <source>Two months ago</source>
        <translation>Két hónappal ezelőtt</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="62"/>
        <source>Three months ago</source>
        <translation>Három hónappal ezelőtt</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="63"/>
        <source>Four months ago</source>
        <translation>Négy hónappal ezelőtt</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="64"/>
        <source>Five months ago</source>
        <translation>Öt hónappal ezelőtt</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="65"/>
        <source>Six months ago</source>
        <translation>Hat hónappal ezelőtt</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="66"/>
        <source>Seven months ago</source>
        <translation>Hét hónappal ezelőtt</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="67"/>
        <source>Eight months ago</source>
        <translation>Nyolc hónappal ezelőtt</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="68"/>
        <source>Nine months ago</source>
        <translation>Kilenc hónappal ezelőtt</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="69"/>
        <source>Ten months ago</source>
        <translation>Tíz hónappal ezelőtt</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="70"/>
        <source>Eleven months ago</source>
        <translation>Tizenegy hónappal ezelőtt</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="71"/>
        <source>Twelve months ago</source>
        <translation>Tizenkét hónappal ezelőtt</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="72"/>
        <source>A year ago</source>
        <translation>Egy évvel ezelőtt</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="73"/>
        <source>Very long time ago</source>
        <translation>Nagyon régen</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="74"/>
        <source>Never</source>
        <translation>Soha</translation>
    </message>
</context>
<context>
    <name>InstantMessagingModel</name>
    <message>
        <location filename="../src/media/textrecording.cpp" line="816"/>
        <source>Me</source>
        <translation>Én</translation>
    </message>
</context>
<context>
    <name>MacroModel</name>
    <message>
        <location filename="../src/macromodel.cpp" line="157"/>
        <source>Macros</source>
        <translation>Makrók</translation>
    </message>
    <message>
        <location filename="../src/macromodel.cpp" line="263"/>
        <source>New</source>
        <translation>Új</translation>
    </message>
    <message>
        <location filename="../src/macromodel.cpp" line="264"/>
        <source>Other</source>
        <translation>Másik</translation>
    </message>
</context>
<context>
    <name>MacroModelPrivate</name>
    <message>
        <location filename="../src/macromodel.cpp" line="77"/>
        <source>Other</source>
        <translation>Másik</translation>
    </message>
</context>
<context>
    <name>NumberCategoryModel</name>
    <message>
        <location filename="../src/numbercategorymodel.cpp" line="56"/>
        <source>Uncategorized</source>
        <translation>Kategorizálatlan</translation>
    </message>
</context>
<context>
    <name>PersonModel</name>
    <message>
        <location filename="../src/personmodel.cpp" line="170"/>
        <source>Persons</source>
        <translation>Személyek</translation>
    </message>
</context>
<context>
    <name>PhoneDirectoryModel</name>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="234"/>
        <source>This account does not support presence tracking</source>
        <translation>Ez a fiók nem támogatja a jelenlét követését</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="237"/>
        <source>No associated account</source>
        <translation>Nincs társított fiók</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="305"/>
        <source>URI</source>
        <translation>URI</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="305"/>
        <source>Type</source>
        <translation>Típus</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="305"/>
        <source>Person</source>
        <translation>Személy</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="305"/>
        <source>Account</source>
        <translation>Fiók</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="305"/>
        <source>State</source>
        <translation>Állapot</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="305"/>
        <source>Call count</source>
        <translation>Hívásszámlálás</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="305"/>
        <source>Week count</source>
        <translation>Hét számlálás</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="306"/>
        <source>Trimester count</source>
        <translation>Negyedév számlálás</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="306"/>
        <source>Have Called</source>
        <translation>Hívott</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="306"/>
        <source>Last used</source>
        <translation>Utoljára használt</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="306"/>
        <source>Name_count</source>
        <translation>Névszám</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="306"/>
        <source>Total (in seconds)</source>
        <translation>Összesen (másodperc)</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="306"/>
        <source>Popularity_index</source>
        <translation>Népszerűségi index</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="307"/>
        <source>Bookmarked</source>
        <translation>Könyvjelzőzött</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="307"/>
        <source>Tracked</source>
        <translation>Nyomon követett</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="307"/>
        <source>Present</source>
        <translation>Jelenlegi</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="307"/>
        <source>Presence message</source>
        <translation>Jelenlét üzenet</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="307"/>
        <source>UID</source>
        <translation>Egyedi azonosító</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="307"/>
        <source>Has certificate</source>
        <translation>Van tanúsítvány</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="307"/>
        <source>Registered name</source>
        <translation>Regisztrált név</translation>
    </message>
</context>
<context>
    <name>PresenceStatusModel</name>
    <message>
        <location filename="../src/presencestatusmodel.cpp" line="197"/>
        <source>Name</source>
        <translation>Név</translation>
    </message>
    <message>
        <location filename="../src/presencestatusmodel.cpp" line="197"/>
        <source>Message</source>
        <translation>Üzenet</translation>
    </message>
    <message>
        <location filename="../src/presencestatusmodel.cpp" line="197"/>
        <source>Color</source>
        <translation>Szín</translation>
    </message>
    <message>
        <location filename="../src/presencestatusmodel.cpp" line="197"/>
        <source>Present</source>
        <translation>Jelenlegi</translation>
    </message>
    <message>
        <location filename="../src/presencestatusmodel.cpp" line="197"/>
        <source>Default</source>
        <translation>Alapértelmezett</translation>
    </message>
    <message>
        <location filename="../src/presencestatusmodel.cpp" line="308"/>
        <location filename="../src/presencestatusmodel.cpp" line="361"/>
        <source>Custom</source>
        <translation>Szokás</translation>
    </message>
    <message>
        <location filename="../src/presencestatusmodel.cpp" line="308"/>
        <location filename="../src/presencestatusmodel.cpp" line="310"/>
        <location filename="../src/presencestatusmodel.cpp" line="354"/>
        <location filename="../src/presencestatusmodel.cpp" line="361"/>
        <source>N/A</source>
        <translation>Nem alkalmazható</translation>
    </message>
</context>
<context>
    <name>ProfileModel</name>
    <message>
        <location filename="../src/profilemodel.cpp" line="637"/>
        <source>Profiles</source>
        <translation>Profilok</translation>
    </message>
</context>
<context>
    <name>ProfileModelPrivate</name>
    <message>
        <location filename="../src/profilemodel.cpp" line="751"/>
        <source>New profile</source>
        <translation>Új profil</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/certificate.cpp" line="42"/>
        <source>Has a private key</source>
        <translation>Van egy személyes kulcs</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="43"/>
        <source>Is not expired</source>
        <translation>Nem járt le</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="44"/>
        <source>Has strong signing</source>
        <translation>Jelentős aláírással rendelkezik</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="45"/>
        <source>Is not self signed</source>
        <translation>Nem önálló aláírás</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="46"/>
        <source>Have a matching key pair</source>
        <translation>Van egy megfelelő kulcspár</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="47"/>
        <source>Has the right private key file permissions</source>
        <translation>Megfelelő személyes kulcs fájl jogosultságokkal rendelkezik</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="48"/>
        <source>Has the right public key file permissions</source>
        <translation>Megfelelő nyilvános kulcs fájl jogosultságokkal rendelkezik</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="49"/>
        <source>Has the right private key directory permissions</source>
        <translation>Megfelelő személyes kulcs könyvtár jogosultságokkal rendelkezik</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="50"/>
        <source>Has the right public key directory permissions</source>
        <translation>Megfelelő nyilvános kulcs könyvtár jogosultságokkal rendelkezik</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="51"/>
        <source>Has the right private key directory location</source>
        <translation>Megvan a megfelelő személyes kulcs könyvtár helye</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="52"/>
        <source>Has the right public key directory location</source>
        <translation>Megvan megfelelő nyilvános kulcs könyvtár helye</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="53"/>
        <source>Has the right private key SELinux attributes</source>
        <translation>Megfelelő személyes kulcs SELinux attribútumokkal rendelkezik</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="54"/>
        <source>Has the right public key SELinux attributes</source>
        <translation>Megfelelő nyilvános kulcs SELinux attribútumokkal rendelkezik</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="55"/>
        <source>The certificate file exist and is readable</source>
        <translation>A tanúsítványfájl létezik és olvasható</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="56"/>
        <source>The file is a valid certificate</source>
        <translation>A fájl egy érvényes tanúsítvány</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="57"/>
        <source>The certificate has a valid authority</source>
        <translation>A tanúsítvány érvényben van</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="58"/>
        <source>The certificate has a known authority</source>
        <translation>A tanúsítványnak van egy ismert kibocsájtója</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="59"/>
        <source>The certificate is not revoked</source>
        <translation>A tanúsítvány nem vonható vissza</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="60"/>
        <source>The certificate authority match</source>
        <translation>A tanúsítvány kibocsátói egyezése</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="61"/>
        <source>The certificate has the expected owner</source>
        <translation>A tanúsítvány rendelkezik a várt tulajdonosával</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="62"/>
        <source>The certificate is within its active period</source>
        <translation>A tanúsítvány az aktív időtartamon belül van</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="90"/>
        <source>Expiration date</source>
        <translation>Lejárati idő</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="91"/>
        <source>Activation date</source>
        <translation>Az aktiválás időpontja</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="92"/>
        <source>Require a private key password</source>
        <translation>Személyes kulcsú jelszó igénylése</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="93"/>
        <source>Public signature</source>
        <translation>Nyilvános aláírás</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="94"/>
        <source>Version</source>
        <translation>Verzió</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="95"/>
        <source>Serial number</source>
        <translation>Sorozatszám</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="96"/>
        <source>Issuer</source>
        <translation>Kibocsátó</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="97"/>
        <source>Subject key algorithm</source>
        <translation>Tárgyi kulcs algoritmus</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="98"/>
        <source>Common name (CN)</source>
        <translation>Közös név (CN)</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="99"/>
        <source>Name (N)</source>
        <translation>Név (N)</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="100"/>
        <source>Organization (O)</source>
        <translation>Szervezet (O)</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="101"/>
        <source>Signature algorithm</source>
        <translation>Aláírási algoritmus</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="102"/>
        <source>Md5 fingerprint</source>
        <translation>MD5 ujjlenyomat</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="103"/>
        <source>Sha1 fingerprint</source>
        <translation>SHA-1 ujjlenyomat</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="104"/>
        <source>Public key ID</source>
        <translation>Nyilvános kulcsazonosító</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="105"/>
        <source>Issuer domain name</source>
        <translation>Kibocsátó domain neve</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="106"/>
        <source>Next expected update</source>
        <translation>Következő várható frissítés</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="107"/>
        <source>Outgoing server</source>
        <translation>Kimenő kiszolgáló</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="183"/>
        <source>Local certificate store</source>
        <translation>Helyi tanúsítványtároló</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="267"/>
        <location filename="../src/localprofilecollection.cpp" line="220"/>
        <source>Default</source>
        <translation>Alapértelmezett</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="267"/>
        <source>Certificate not associated with a group</source>
        <translation>Tanúsítvány nincs társítva egy csoporttal</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="397"/>
        <source>A certificate</source>
        <translation>Tanúsítvány</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="397"/>
        <source>An organisation</source>
        <translation>Egy szervezet</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="420"/>
        <source>Details</source>
        <translation>Részletek</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="420"/>
        <source>The content of the certificate</source>
        <translation>A tanúsítvány tartalma</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="421"/>
        <source>Checks</source>
        <translation>Ellenőrzések</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="421"/>
        <source>Various security related information</source>
        <translation>Különféle biztonsági információk</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="610"/>
        <source>Header</source>
        <translation>Fejléc</translation>
    </message>
    <message>
        <location filename="../src/daemoncertificatecollection.cpp" line="148"/>
        <source>Daemon certificate store</source>
        <translation>Rendszerfolyamat tanúsítványtároló</translation>
    </message>
    <message>
        <location filename="../src/daemoncertificatecollection.cpp" line="152"/>
        <source>%1 banned list</source>
        <extracomment>The list of banned certificates for this account</extracomment>
        <translation>%1 tiltott lista</translation>
    </message>
    <message>
        <location filename="../src/daemoncertificatecollection.cpp" line="155"/>
        <source>%1 allowed list</source>
        <extracomment>The list of allowed certificates for this account</extracomment>
        <translation>%1 engedélyezett lista</translation>
    </message>
    <message>
        <location filename="../src/daemoncertificatecollection.cpp" line="160"/>
        <location filename="../src/foldercertificatecollection.cpp" line="198"/>
        <source>Certificate</source>
        <translation>Tanúsítvány</translation>
    </message>
    <message>
        <location filename="../src/extensions/presencecollectionextension.cpp" line="38"/>
        <source>Presence tracking</source>
        <translation>Jelenlétkövetés</translation>
    </message>
    <message>
        <location filename="../src/extensions/securityevaluationextension.cpp" line="63"/>
        <source>Security evaluation</source>
        <translation>Biztonsági értékelés</translation>
    </message>
    <message>
        <location filename="../src/fallbackpersoncollection.cpp" line="194"/>
        <source>Contact</source>
        <translation>Partnerek</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="532"/>
        <source>Bookmark</source>
        <translation>Könyvjelző</translation>
    </message>
    <message>
        <location filename="../src/localhistorycollection.cpp" line="195"/>
        <source>Local history</source>
        <translation>Helyi előzmények</translation>
    </message>
    <message>
        <location filename="../src/localhistorycollection.cpp" line="200"/>
        <source>History</source>
        <translation>Előzmények</translation>
    </message>
    <message>
        <location filename="../src/localrecordingcollection.cpp" line="101"/>
        <source>Local recordings</source>
        <translation>Helyi felvételek</translation>
    </message>
    <message>
        <location filename="../src/localrecordingcollection.cpp" line="106"/>
        <location filename="../src/localtextrecordingcollection.cpp" line="170"/>
        <source>Recording</source>
        <translation>Felvétel</translation>
    </message>
    <message>
        <location filename="../src/localringtonecollection.cpp" line="220"/>
        <source>Local ringtones</source>
        <translation>Helyi csengőhangok</translation>
    </message>
    <message>
        <location filename="../src/localringtonecollection.cpp" line="225"/>
        <source>Ringtone</source>
        <translation>Csengőhang</translation>
    </message>
    <message>
        <location filename="../src/localtextrecordingcollection.cpp" line="165"/>
        <source>Local text recordings</source>
        <translation>Helyi szöveges felvételek</translation>
    </message>
    <message>
        <location filename="../src/numbercategory.cpp" line="72"/>
        <source>Phone number types</source>
        <translation>Telefonszám típusok</translation>
    </message>
    <message>
        <location filename="../src/numbercategorymodel.cpp" line="186"/>
        <source>Other</source>
        <translation>Másik</translation>
    </message>
    <message>
        <location filename="../src/protocolmodel.cpp" line="50"/>
        <source>Ring Account</source>
        <translation>Jami fiók</translation>
    </message>
    <message>
        <location filename="../src/protocolmodel.cpp" line="51"/>
        <source>SIP Account</source>
        <translation>SIP fiók </translation>
    </message>
    <message>
        <location filename="../src/qtwrapper/callmanager_wrap.h" line="315"/>
        <source>Me</source>
        <translation>Én</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="38"/>
        <source>TLS is disabled, the negotiation won&apos;t be encrypted. Your communication will be vulnerable to snooping</source>
        <translation>A TLS le van tiltva, a tárgyalás nem lesz titkosítva. A közlés sebezhető lesz szaglászással.</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="40"/>
        <source>Your certificate and authority don&apos;t match, if your certificate require an authority, it won&apos;t work</source>
        <translation>A tanúsítvány és a kibocsátó nem egyezik meg, ha a tanúsítványhoz kibocsátó szükséges, akkor nem fog működni.</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="41"/>
        <source>The outgoing server specified doesn&apos;t match the hostname or the one included in the certificate</source>
        <translation>A megadott kimenő kiszolgáló nem egyezik a gazdagépnévvel vagy a tanúsítványban szereplővel.</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="42"/>
        <source>The &quot;verify incoming certificate&quot; option is disabled, this leave you vulnerable to man in the middle attack</source>
        <translation>A „bejövő igazolás ellenőrzése” opció letiltott, ez hagyja, hogy sebezhető legyen az embernek a középső támadásban.</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="43"/>
        <source>The &quot;verify answer certificate&quot; option is disabled, this leave you vulnerable to man in the middle attack</source>
        <translation>A „ellenőrizze a válaszadási tanúsítvány” opciót letiltott, így a középső támadásban az embernek sebezhetőséget okozhat.</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="44"/>
        <source>None of your certificate provide a private key, this is required. Please select a private key or use a certificate with one built-in</source>
        <translation>Az Ön tanúsítványa nem tartalmaz személyes kulcsot, pedig erre szükség van. Kérjük, hogy válasszon egy személyes kulcsot vagy használjon egy tanúsítványt egy beépített eszközzel.</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="47"/>
        <source>No certificate authority is provided, it won&apos;t be possible to validate if the answer certificates are valid. Some account may also not work.</source>
        <translation>Nincs tanúsítvány kibocsátó, nem lehet érvényesíteni, ha a válasz-tanúsítványok érvényesek. Néhány fiók szintén nem működhet.</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="46"/>
        <source>No certificate has been provided. This is, for now, unsupported by Ring</source>
        <translation>Nincs tanúsítvány biztosítva. Ezért, átmenetileg a Jami nem támogatja.</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="37"/>
        <source>Your media streams are not encrypted, please enable SDES</source>
        <translation>A média közvetítései nincsenek titkosítva, kérjük engedélyezze SDES-t.</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="50"/>
        <source>Your certificate is expired, please contact your system administrator.</source>
        <translation>Tanúsítványa lejárt, vegye fel a kapcsolatot a rendszergazdájával.</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="51"/>
        <source>Your certificate is self signed. This break the chain of trust.</source>
        <translation>A tanúsítvány önaláíró. Ez megtöri a bizalom láncolatát.</translation>
    </message>
    <message>
        <location filename="../src/tlsmethodmodel.cpp" line="64"/>
        <source>Default</source>
        <comment>Default TLS protocol version</comment>
        <translation>Alapértelmezett</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="517"/>
        <location filename="../src/useractionmodel.cpp" line="759"/>
        <source>Accept</source>
        <translation>Elfogadás</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="60"/>
        <location filename="../src/useractionmodel.cpp" line="518"/>
        <location filename="../src/useractionmodel.cpp" line="771"/>
        <source>Hold</source>
        <translation>Tartás</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="62"/>
        <source>Talking</source>
        <translation>Hívásban</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="64"/>
        <source>ERROR</source>
        <translation>HIBA</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="66"/>
        <source>Incoming</source>
        <translation>Bejövő</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="68"/>
        <source>Calling</source>
        <translation>Hívás</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="70"/>
        <source>Connecting</source>
        <translation>Kapcsolódás</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="72"/>
        <source>Searching</source>
        <translation>Keresés</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="74"/>
        <source>Inactive</source>
        <translation>Tétlen</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="76"/>
        <location filename="../src/api/call.h" line="82"/>
        <source>Finished</source>
        <translation>Befejezett</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="78"/>
        <source>Timeout</source>
        <translation>Időtúllépés</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="80"/>
        <source>Peer busy</source>
        <translation>Partner elfoglalt</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="84"/>
        <source>Communication established</source>
        <translation>A közlés létrejött</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="519"/>
        <source>Mute audio</source>
        <translation>Hang némítása</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="520"/>
        <source>Mute video</source>
        <translation>Videó némítása</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="521"/>
        <source>Server transfer</source>
        <translation>Kiszolgálóátvitel</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="522"/>
        <source>Record</source>
        <translation>Felvétel</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="523"/>
        <location filename="../src/useractionmodel.cpp" line="789"/>
        <source>Hangup</source>
        <translation>Lerak</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="524"/>
        <source>Join</source>
        <translation>Csatlakozás</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="525"/>
        <source>Add new</source>
        <translation>Új hozzáadása</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="526"/>
        <source>Toggle video</source>
        <translation>Videó be-/kikapcsolása</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="527"/>
        <source>Add a contact</source>
        <translation>Partner hozzáadása</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="528"/>
        <source>Add to existing contact</source>
        <translation>Hozzáadás meglévő kapcsolathoz</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="529"/>
        <source>Delete contact</source>
        <translation>Partner törlése</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="530"/>
        <source>Email contact</source>
        <translation>E-mail kapcsolat</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="531"/>
        <source>Copy contact</source>
        <translation>Kapcsolat másolása</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="534"/>
        <source>Add phone number</source>
        <translation>Telefonszám hozzáadása</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="535"/>
        <source>Call again</source>
        <translation>Újra hívás</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="536"/>
        <source>Edit contact details</source>
        <translation>Kapcsolatfelvétel szerkesztése</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="537"/>
        <source>Remove from history</source>
        <translation>Eltávolítás az előzményekből</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="786"/>
        <source>Remove</source>
        <translation>Eltávolítás</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="756"/>
        <source>Call</source>
        <translation>Hívás</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="533"/>
        <source>Open chat</source>
        <translation>Csevegés megnyitása</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="768"/>
        <source>Unhold</source>
        <translation>Tartás megszüntetése</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="779"/>
        <source>Cancel</source>
        <translation>Mégse</translation>
    </message>
    <message>
        <location filename="../src/localprofilecollection.cpp" line="147"/>
        <source>Local profiles</source>
        <translation>Helyi profil</translation>
    </message>
    <message>
        <location filename="../src/localprofilecollection.cpp" line="152"/>
        <source>Profile Collection</source>
        <translation>Profilgyűjtemény</translation>
    </message>
    <message>
        <location filename="../src/peerprofilecollection.cpp" line="141"/>
        <source>Peer profiles</source>
        <translation>Partnerprofilok</translation>
    </message>
    <message>
        <location filename="../src/peerprofilecollection.cpp" line="146"/>
        <source>Peers Profiles Collection</source>
        <translation>Partnerprofil gyűjtemény</translation>
    </message>
    <message>
        <location filename="../src/conversationmodel.cpp" line="1265"/>
        <location filename="../src/conversationmodel.cpp" line="1649"/>
        <location filename="../src/conversationmodel.cpp" line="1909"/>
        <source>Invitation received</source>
        <translation>Meghívó érkezett</translation>
    </message>
    <message>
        <location filename="../src/conversationmodel.cpp" line="1266"/>
        <source>Contact added</source>
        <translation>Kapcsolat hozzáadva</translation>
    </message>
    <message>
        <location filename="../src/conversationmodel.cpp" line="1308"/>
        <location filename="../src/conversationmodel.cpp" line="1315"/>
        <source>Invitation accepted</source>
        <translation>Meghívó elfogadva</translation>
    </message>
    <message>
        <location filename="../src/conversationmodel.cpp" line="1524"/>
        <source>ð Outgoing call</source>
        <translation>ð Kimenő hívás</translation>
    </message>
    <message>
        <location filename="../src/conversationmodel.cpp" line="1526"/>
        <source>ð Incoming call</source>
        <translation>ð Bejövő hívás</translation>
    </message>
    <message>
        <location filename="../src/conversationmodel.cpp" line="1539"/>
        <source>ð Outgoing call - </source>
        <translation>ð Kimenő hívás - </translation>
    </message>
    <message>
        <location filename="../src/conversationmodel.cpp" line="1542"/>
        <source>ð Incoming call - </source>
        <translation>ð Bejövő hívás - </translation>
    </message>
    <message>
        <location filename="../src/conversationmodel.cpp" line="1546"/>
        <source>ð½ Missed outgoing call</source>
        <translation>ð½ Nem fogadott kimenő hívás</translation>
    </message>
    <message>
        <location filename="../src/conversationmodel.cpp" line="1548"/>
        <source>ð½ Missed incoming call</source>
        <translation>ð½ Nem fogadott bejövő hívás</translation>
    </message>
</context>
<context>
    <name>RingDeviceModel</name>
    <message>
        <location filename="../src/ringdevicemodel.cpp" line="110"/>
        <source>ID</source>
        <translation>Azonosító</translation>
    </message>
    <message>
        <location filename="../src/ringdevicemodel.cpp" line="112"/>
        <source>Name</source>
        <translation>Név</translation>
    </message>
</context>
<context>
    <name>SecurityEvaluationModelPrivate</name>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="683"/>
        <source>Authority</source>
        <translation>Felhatalmazás</translation>
    </message>
</context>
<context>
    <name>TlsMethodModel</name>
    <message>
        <location filename="../src/tlsmethodmodel.cpp" line="101"/>
        <source>Automatic</source>
        <translation>Automatikus</translation>
    </message>
</context>
<context>
    <name>Video::SourceModel</name>
    <message>
        <location filename="../src/video/sourcemodel.cpp" line="95"/>
        <source>NONE</source>
        <translation>EGYIK SEM</translation>
    </message>
    <message>
        <location filename="../src/video/sourcemodel.cpp" line="101"/>
        <source>SCREEN</source>
        <translation>KÉPERNYŐ</translation>
    </message>
    <message>
        <location filename="../src/video/sourcemodel.cpp" line="107"/>
        <source>FILE</source>
        <translation>FÁJL</translation>
    </message>
</context>
<context>
    <name>lrc::ContactModelPimpl</name>
    <message>
        <location filename="../src/contactmodel.cpp" line="393"/>
        <source>Searchingâ¦</source>
        <translation>Keresésâ¦</translation>
    </message>
    <message>
        <location filename="../src/contactmodel.cpp" line="747"/>
        <source>Invalid ID</source>
        <translation>Érvénytelen azonosító</translation>
    </message>
    <message>
        <location filename="../src/contactmodel.cpp" line="750"/>
        <source>Not found</source>
        <translation>Nem található</translation>
    </message>
    <message>
        <location filename="../src/contactmodel.cpp" line="753"/>
        <source>Couldn&apos;t lookupâ¦</source>
        <translation>Betekintés nem találhatókâ¦</translation>
    </message>
</context>
<context>
    <name>lrc::api::ContactModel</name>
    <message>
        <location filename="../src/contactmodel.cpp" line="364"/>
        <source>Bad URI scheme</source>
        <translation>Érvénytelen URI-séma</translation>
    </message>
</context>
<context>
    <name>media::RecordingModel</name>
    <message>
        <location filename="../src/media/recordingmodel.cpp" line="189"/>
        <source>Recordings</source>
        <translation>Felvételek</translation>
    </message>
    <message>
        <location filename="../src/media/recordingmodel.cpp" line="259"/>
        <source>Text messages</source>
        <translation>Szöveges üzenetek</translation>
    </message>
    <message>
        <location filename="../src/media/recordingmodel.cpp" line="264"/>
        <source>Audio/Video</source>
        <translation>Hang/videó</translation>
    </message>
</context>
</TS>