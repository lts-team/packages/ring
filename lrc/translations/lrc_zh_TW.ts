<?xml version="1.0" ?><!DOCTYPE TS><TS language="zh_TW" sourcelanguage="en" version="2.0">
<context>
    <name>Account</name>
    <message>
        <location filename="../src/account.cpp" line="307"/>
        <source>Ready</source>
        <extracomment>Account state</extracomment>
        <translation>準備</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="309"/>
        <source>Registered</source>
        <extracomment>Account state</extracomment>
        <translation>已註冊</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="313"/>
        <source>Initializing</source>
        <extracomment>Account state</extracomment>
        <translation>正在初始化</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="317"/>
        <source>Error</source>
        <extracomment>Account state</extracomment>
        <translation>錯誤</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="321"/>
        <source>Network unreachable</source>
        <extracomment>Account state</extracomment>
        <translation>網路無法連線</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="323"/>
        <source>Host unreachable</source>
        <extracomment>Account state</extracomment>
        <translation>主機無法連線</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="311"/>
        <source>Not registered</source>
        <extracomment>Account state</extracomment>
        <translation>未註冊</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="315"/>
        <source>Tryingâ¦</source>
        <extracomment>Account state</extracomment>
        <translation>正在嘗試</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="319"/>
        <source>Authentication failed</source>
        <extracomment>Account state</extracomment>
        <translation>驗證失敗</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="325"/>
        <source>STUN configuration error</source>
        <extracomment>Account state</extracomment>
        <translation>STUN 設定錯誤</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="327"/>
        <source>STUN server invalid</source>
        <extracomment>Account state</extracomment>
        <translation>STUN 伺服器無效</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="329"/>
        <source>Service unavailable</source>
        <extracomment>Account state</extracomment>
        <translation>服務不可用</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="331"/>
        <source>Unacceptable</source>
        <extracomment>Account state</extracomment>
        <translation>無法接受</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="333"/>
        <source>Invalid</source>
        <extracomment>Account state</extracomment>
        <translation>無效</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="335"/>
        <source>Request timeout</source>
        <extracomment>Account state</extracomment>
        <translation>請求逾時</translation>
    </message>
</context>
<context>
    <name>AccountChecksModel</name>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="438"/>
        <source>Configuration</source>
        <translation>設定</translation>
    </message>
</context>
<context>
    <name>Call</name>
    <message>
        <location filename="../src/call.cpp" line="719"/>
        <source>New</source>
        <extracomment>Call state</extracomment>
        <translation>新</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="722"/>
        <source>Ringing</source>
        <extracomment>Call state</extracomment>
        <translation>響鈴中</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="725"/>
        <source>Calling</source>
        <extracomment>Call state</extracomment>
        <translation>正在通話</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="728"/>
        <source>Talking</source>
        <extracomment>Call state</extracomment>
        <translation>通話中</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="731"/>
        <source>Dialing</source>
        <extracomment>Call state</extracomment>
        <translation>正在撥號</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="734"/>
        <source>Hold</source>
        <extracomment>Call state</extracomment>
        <translation>暫停通話</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="737"/>
        <source>Failed</source>
        <extracomment>Call state</extracomment>
        <translation>失敗</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="740"/>
        <source>Busy</source>
        <extracomment>Call state</extracomment>
        <translation>忙碌</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="743"/>
        <source>Transfer</source>
        <extracomment>Call state</extracomment>
        <translation>轉送</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="746"/>
        <source>Transfer hold</source>
        <extracomment>Call state</extracomment>
        <translation>轉送保留</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="749"/>
        <source>Over</source>
        <extracomment>Call state</extracomment>
        <translation>結束</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="752"/>
        <source>Error</source>
        <extracomment>Call state</extracomment>
        <translation>錯誤</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="755"/>
        <location filename="../src/call.cpp" line="860"/>
        <source>Conference</source>
        <extracomment>Call state</extracomment>
        <translation>會議</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="758"/>
        <source>Conference (hold)</source>
        <extracomment>Call state</extracomment>
        <translation>會議（保留）</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="761"/>
        <source>ERROR</source>
        <extracomment>Call state</extracomment>
        <translation>錯誤</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="764"/>
        <source>Searching for</source>
        <extracomment>Call state</extracomment>
        <translation>搜尋</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="767"/>
        <source>Aborted</source>
        <extracomment>Call state</extracomment>
        <translation>已中止</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="770"/>
        <source>Communication established</source>
        <extracomment>Call state</extracomment>
        <translation>已建立通訊</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="864"/>
        <source>Unknown</source>
        <translation>未知</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="2231"/>
        <source>Account: </source>
        <translation>帳號：</translation>
    </message>
</context>
<context>
    <name>CallModel</name>
    <message>
        <location filename="../src/callmodel.cpp" line="898"/>
        <source>Calls</source>
        <translation>通話</translation>
    </message>
</context>
<context>
    <name>CallModelPrivate</name>
    <message>
        <location filename="../src/callmodel.cpp" line="533"/>
        <location filename="../src/callmodel.cpp" line="558"/>
        <source>Invalid account</source>
        <translation>無效的帳號</translation>
    </message>
</context>
<context>
    <name>CallPrivate</name>
    <message>
        <location filename="../src/call.cpp" line="1795"/>
        <source>Aborted</source>
        <translation>已中止</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="1816"/>
        <source>No account registered!</source>
        <translation>沒有已註冊的帳號！</translation>
    </message>
</context>
<context>
    <name>CategorizedBookmarkModel</name>
    <message>
        <location filename="../src/categorizedbookmarkmodel.cpp" line="181"/>
        <source>Most popular</source>
        <extracomment>Most popular contacts</extracomment>
        <translation>最流行的</translation>
    </message>
    <message>
        <location filename="../src/categorizedbookmarkmodel.cpp" line="299"/>
        <source>Contacts</source>
        <translation>聯絡人</translation>
    </message>
</context>
<context>
    <name>CategorizedContactModel</name>
    <message>
        <location filename="../src/categorizedcontactmodel.cpp" line="401"/>
        <source>Contacts</source>
        <translation>聯絡人</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="129"/>
        <source>Empty</source>
        <translation> 空的</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="135"/>
        <location filename="../src/private/sortproxies.cpp" line="153"/>
        <source>Unknown</source>
        <translation>未知</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="141"/>
        <source>Never</source>
        <translation>永不</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="147"/>
        <source>Other</source>
        <translation>其他</translation>
    </message>
</context>
<context>
    <name>CategorizedHistoryModel</name>
    <message>
        <location filename="../src/categorizedhistorymodel.cpp" line="412"/>
        <source>History</source>
        <translation>歷史記錄</translation>
    </message>
</context>
<context>
    <name>ChainOfTrustModel</name>
    <message>
        <location filename="../src/chainoftrustmodel.cpp" line="173"/>
        <source>Chain of trust</source>
        <translation>信任鏈</translation>
    </message>
</context>
<context>
    <name>CollectionModel</name>
    <message>
        <location filename="../src/collectionmodel.cpp" line="279"/>
        <source>Name</source>
        <translation>名稱</translation>
    </message>
</context>
<context>
    <name>ContactSortingCategoryModel</name>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="49"/>
        <source>Name</source>
        <translation>名稱</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="50"/>
        <source>Organisation</source>
        <translation>組織</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="51"/>
        <source>Recently used</source>
        <translation>最近使用</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="52"/>
        <source>Group</source>
        <translation>群組</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="53"/>
        <source>Department</source>
        <translation>部門</translation>
    </message>
</context>
<context>
    <name>HistorySortingCategoryModel</name>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="57"/>
        <source>Date</source>
        <translation>日期</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="58"/>
        <source>Name</source>
        <translation>名稱</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="59"/>
        <source>Popularity</source>
        <translation>聲望</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="60"/>
        <source>Duration</source>
        <translation>持續時間</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="61"/>
        <source>Total time</source>
        <translation>總時間</translation>
    </message>
</context>
<context>
    <name>HistoryTimeCategoryModel</name>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="50"/>
        <source>Today</source>
        <translation>今天</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="51"/>
        <source>Yesterday</source>
        <translation>昨天</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="58"/>
        <source>Two weeks ago</source>
        <translation>兩週前</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="59"/>
        <source>Three weeks ago</source>
        <translation>三週前</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="57"/>
        <source>A week ago</source>
        <translation>一週前</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="60"/>
        <source>A month ago</source>
        <translation>一個月前</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="61"/>
        <source>Two months ago</source>
        <translation>兩個月前</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="62"/>
        <source>Three months ago</source>
        <translation>三個月前</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="63"/>
        <source>Four months ago</source>
        <translation>四個月前</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="64"/>
        <source>Five months ago</source>
        <translation>五個月前</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="65"/>
        <source>Six months ago</source>
        <translation>六個月前</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="66"/>
        <source>Seven months ago</source>
        <translation>七個月前</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="67"/>
        <source>Eight months ago</source>
        <translation>八個月前</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="68"/>
        <source>Nine months ago</source>
        <translation>九個月前</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="69"/>
        <source>Ten months ago</source>
        <translation>十個月前</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="70"/>
        <source>Eleven months ago</source>
        <translation>十一個月前</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="71"/>
        <source>Twelve months ago</source>
        <translation>十二個月前</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="72"/>
        <source>A year ago</source>
        <translation>一年前</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="73"/>
        <source>Very long time ago</source>
        <translation>非常久以前</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="74"/>
        <source>Never</source>
        <translation>永不</translation>
    </message>
</context>
<context>
    <name>InstantMessagingModel</name>
    <message>
        <location filename="../src/media/textrecording.cpp" line="816"/>
        <source>Me</source>
        <translation>我</translation>
    </message>
</context>
<context>
    <name>MacroModel</name>
    <message>
        <location filename="../src/macromodel.cpp" line="157"/>
        <source>Macros</source>
        <translation>巨集</translation>
    </message>
    <message>
        <location filename="../src/macromodel.cpp" line="263"/>
        <source>New</source>
        <translation>新</translation>
    </message>
    <message>
        <location filename="../src/macromodel.cpp" line="264"/>
        <source>Other</source>
        <translation>其他</translation>
    </message>
</context>
<context>
    <name>MacroModelPrivate</name>
    <message>
        <location filename="../src/macromodel.cpp" line="77"/>
        <source>Other</source>
        <translation>其他</translation>
    </message>
</context>
<context>
    <name>NumberCategoryModel</name>
    <message>
        <location filename="../src/numbercategorymodel.cpp" line="56"/>
        <source>Uncategorized</source>
        <translation>未分類</translation>
    </message>
</context>
<context>
    <name>PersonModel</name>
    <message>
        <location filename="../src/personmodel.cpp" line="170"/>
        <source>Persons</source>
        <translation>個人</translation>
    </message>
</context>
<context>
    <name>PhoneDirectoryModel</name>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="234"/>
        <source>This account does not support presence tracking</source>
        <translation>此帳號不支援上線追蹤</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="237"/>
        <source>No associated account</source>
        <translation>沒有關聯的帳號</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="305"/>
        <source>URI</source>
        <translation>URI</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="305"/>
        <source>Type</source>
        <translation>類型</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="305"/>
        <source>Person</source>
        <translation>個人</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="305"/>
        <source>Account</source>
        <translation>帳號</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="305"/>
        <source>State</source>
        <translation>狀態</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="305"/>
        <source>Call count</source>
        <translation>通話計數</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="305"/>
        <source>Week count</source>
        <translation>週計數</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="306"/>
        <source>Trimester count</source>
        <translation>季計數</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="306"/>
        <source>Have Called</source>
        <translation>已通話</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="306"/>
        <source>Last used</source>
        <translation>最後使用</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="306"/>
        <source>Name_count</source>
        <translation>名稱計數</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="306"/>
        <source>Total (in seconds)</source>
        <translation>總共（以秒計）</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="306"/>
        <source>Popularity_index</source>
        <translation>人氣指數</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="307"/>
        <source>Bookmarked</source>
        <translation>已加入書籤</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="307"/>
        <source>Tracked</source>
        <translation>已追蹤</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="307"/>
        <source>Present</source>
        <translation>目前</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="307"/>
        <source>Presence message</source>
        <translation>存在訊息</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="307"/>
        <source>UID</source>
        <translation>UID</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="307"/>
        <source>Has certificate</source>
        <translation>有證書</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="307"/>
        <source>Registered name</source>
        <translation>已註冊的名稱</translation>
    </message>
</context>
<context>
    <name>PresenceStatusModel</name>
    <message>
        <location filename="../src/presencestatusmodel.cpp" line="197"/>
        <source>Name</source>
        <translation>名稱</translation>
    </message>
    <message>
        <location filename="../src/presencestatusmodel.cpp" line="197"/>
        <source>Message</source>
        <translation>訊息</translation>
    </message>
    <message>
        <location filename="../src/presencestatusmodel.cpp" line="197"/>
        <source>Color</source>
        <translation>色彩</translation>
    </message>
    <message>
        <location filename="../src/presencestatusmodel.cpp" line="197"/>
        <source>Present</source>
        <translation>目前</translation>
    </message>
    <message>
        <location filename="../src/presencestatusmodel.cpp" line="197"/>
        <source>Default</source>
        <translation>預設</translation>
    </message>
    <message>
        <location filename="../src/presencestatusmodel.cpp" line="308"/>
        <location filename="../src/presencestatusmodel.cpp" line="361"/>
        <source>Custom</source>
        <translation>自訂</translation>
    </message>
    <message>
        <location filename="../src/presencestatusmodel.cpp" line="308"/>
        <location filename="../src/presencestatusmodel.cpp" line="310"/>
        <location filename="../src/presencestatusmodel.cpp" line="354"/>
        <location filename="../src/presencestatusmodel.cpp" line="361"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
</context>
<context>
    <name>ProfileModel</name>
    <message>
        <location filename="../src/profilemodel.cpp" line="637"/>
        <source>Profiles</source>
        <translation>介紹</translation>
    </message>
</context>
<context>
    <name>ProfileModelPrivate</name>
    <message>
        <location filename="../src/profilemodel.cpp" line="751"/>
        <source>New profile</source>
        <translation>新介紹</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/certificate.cpp" line="42"/>
        <source>Has a private key</source>
        <translation>有私密金鑰</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="43"/>
        <source>Is not expired</source>
        <translation>未過期</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="44"/>
        <source>Has strong signing</source>
        <translation>有高強度的簽章</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="45"/>
        <source>Is not self signed</source>
        <translation>非自行簽發</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="46"/>
        <source>Have a matching key pair</source>
        <translation>有相符的金鑰配對</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="47"/>
        <source>Has the right private key file permissions</source>
        <translation>有對的私密金鑰檔案權限</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="48"/>
        <source>Has the right public key file permissions</source>
        <translation>有對的公開金鑰檔案權限</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="49"/>
        <source>Has the right private key directory permissions</source>
        <translation>有對的私密金鑰目錄權限</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="50"/>
        <source>Has the right public key directory permissions</source>
        <translation>有對的公開金鑰目錄權限</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="51"/>
        <source>Has the right private key directory location</source>
        <translation>有對的私密金鑰目錄位置</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="52"/>
        <source>Has the right public key directory location</source>
        <translation>有對的公開金鑰目錄位置</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="53"/>
        <source>Has the right private key SELinux attributes</source>
        <translation>有對的私密金鑰 SELinux 屬性</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="54"/>
        <source>Has the right public key SELinux attributes</source>
        <translation>有對的公開金鑰 SELinux 屬性</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="55"/>
        <source>The certificate file exist and is readable</source>
        <translation>證書檔案存在且可讀取</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="56"/>
        <source>The file is a valid certificate</source>
        <translation>檔案是有效的證書</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="57"/>
        <source>The certificate has a valid authority</source>
        <translation>證書有有效的機構</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="58"/>
        <source>The certificate has a known authority</source>
        <translation>證書有已知的機構</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="59"/>
        <source>The certificate is not revoked</source>
        <translation>證書不可撤銷</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="60"/>
        <source>The certificate authority match</source>
        <translation>證書機構相符</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="61"/>
        <source>The certificate has the expected owner</source>
        <translation>證書有預期的擁有者</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="62"/>
        <source>The certificate is within its active period</source>
        <translation>證書在其效期內</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="90"/>
        <source>Expiration date</source>
        <translation>過期日</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="91"/>
        <source>Activation date</source>
        <translation>有效日期</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="92"/>
        <source>Require a private key password</source>
        <translation>需要私密金鑰密碼</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="93"/>
        <source>Public signature</source>
        <translation>公開簽章</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="94"/>
        <source>Version</source>
        <translation>版本</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="95"/>
        <source>Serial number</source>
        <translation>序列號</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="96"/>
        <source>Issuer</source>
        <translation>發行者</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="97"/>
        <source>Subject key algorithm</source>
        <translation>主體金鑰演算法</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="98"/>
        <source>Common name (CN)</source>
        <translation>通用名稱 (CN)</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="99"/>
        <source>Name (N)</source>
        <translation>名稱 (N)</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="100"/>
        <source>Organization (O)</source>
        <translation>組織/單位 (O)</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="101"/>
        <source>Signature algorithm</source>
        <translation>數位簽章演算法</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="102"/>
        <source>Md5 fingerprint</source>
        <translation>MD5 指紋</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="103"/>
        <source>Sha1 fingerprint</source>
        <translation>SHA1 指紋</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="104"/>
        <source>Public key ID</source>
        <translation>公開金鑰 ID</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="105"/>
        <source>Issuer domain name</source>
        <translation>發行網域名稱</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="106"/>
        <source>Next expected update</source>
        <translation>預計下一次更新</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="107"/>
        <source>Outgoing server</source>
        <translation>外連伺服器</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="183"/>
        <source>Local certificate store</source>
        <translation>本機證書儲存</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="267"/>
        <location filename="../src/localprofilecollection.cpp" line="220"/>
        <source>Default</source>
        <translation>預設</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="267"/>
        <source>Certificate not associated with a group</source>
        <translation>與組織無關的證書</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="397"/>
        <source>A certificate</source>
        <translation>證書</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="397"/>
        <source>An organisation</source>
        <translation>組織</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="420"/>
        <source>Details</source>
        <translation>詳細資料</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="420"/>
        <source>The content of the certificate</source>
        <translation>證書內容</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="421"/>
        <source>Checks</source>
        <translation>檢查</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="421"/>
        <source>Various security related information</source>
        <translation>各種安全相關資訊</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="610"/>
        <source>Header</source>
        <translation>標頭</translation>
    </message>
    <message>
        <location filename="../src/daemoncertificatecollection.cpp" line="148"/>
        <source>Daemon certificate store</source>
        <translation>幕後證書儲存</translation>
    </message>
    <message>
        <location filename="../src/daemoncertificatecollection.cpp" line="152"/>
        <source>%1 banned list</source>
        <extracomment>The list of banned certificates for this account</extracomment>
        <translation>%1 黑名單</translation>
    </message>
    <message>
        <location filename="../src/daemoncertificatecollection.cpp" line="155"/>
        <source>%1 allowed list</source>
        <extracomment>The list of allowed certificates for this account</extracomment>
        <translation>%1 白名單</translation>
    </message>
    <message>
        <location filename="../src/daemoncertificatecollection.cpp" line="160"/>
        <location filename="../src/foldercertificatecollection.cpp" line="198"/>
        <source>Certificate</source>
        <translation>證書</translation>
    </message>
    <message>
        <location filename="../src/extensions/presencecollectionextension.cpp" line="38"/>
        <source>Presence tracking</source>
        <translation>存在追蹤</translation>
    </message>
    <message>
        <location filename="../src/extensions/securityevaluationextension.cpp" line="63"/>
        <source>Security evaluation</source>
        <translation>安全評估</translation>
    </message>
    <message>
        <location filename="../src/fallbackpersoncollection.cpp" line="194"/>
        <source>Contact</source>
        <translation>聯絡人</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="532"/>
        <source>Bookmark</source>
        <translation>書籤</translation>
    </message>
    <message>
        <location filename="../src/localhistorycollection.cpp" line="195"/>
        <source>Local history</source>
        <translation>本機歷史紀錄</translation>
    </message>
    <message>
        <location filename="../src/localhistorycollection.cpp" line="200"/>
        <source>History</source>
        <translation>記錄</translation>
    </message>
    <message>
        <location filename="../src/localrecordingcollection.cpp" line="101"/>
        <source>Local recordings</source>
        <translation>本機記錄</translation>
    </message>
    <message>
        <location filename="../src/localrecordingcollection.cpp" line="106"/>
        <location filename="../src/localtextrecordingcollection.cpp" line="170"/>
        <source>Recording</source>
        <translation>記錄</translation>
    </message>
    <message>
        <location filename="../src/localringtonecollection.cpp" line="220"/>
        <source>Local ringtones</source>
        <translation>本機鈴聲</translation>
    </message>
    <message>
        <location filename="../src/localringtonecollection.cpp" line="225"/>
        <source>Ringtone</source>
        <translation>鈴聲</translation>
    </message>
    <message>
        <location filename="../src/localtextrecordingcollection.cpp" line="165"/>
        <source>Local text recordings</source>
        <translation>本機文字記錄</translation>
    </message>
    <message>
        <location filename="../src/numbercategory.cpp" line="72"/>
        <source>Phone number types</source>
        <translation>電話號碼類型</translation>
    </message>
    <message>
        <location filename="../src/numbercategorymodel.cpp" line="186"/>
        <source>Other</source>
        <translation>其他</translation>
    </message>
    <message>
        <location filename="../src/protocolmodel.cpp" line="50"/>
        <source>Ring Account</source>
        <translation>Ring 帳號</translation>
    </message>
    <message>
        <location filename="../src/protocolmodel.cpp" line="51"/>
        <source>SIP Account</source>
        <translation>SIP 帳號</translation>
    </message>
    <message>
        <location filename="../src/qtwrapper/callmanager_wrap.h" line="315"/>
        <source>Me</source>
        <translation>我</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="38"/>
        <source>TLS is disabled, the negotiation won&apos;t be encrypted. Your communication will be vulnerable to snooping</source>
        <translation>TLS 已停用，協商將不會加密。您的通訊將會很容易被窺探</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="40"/>
        <source>Your certificate and authority don&apos;t match, if your certificate require an authority, it won&apos;t work</source>
        <translation>您的證書與機構不相符，若您的證書需要機構，它就無法運作</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="41"/>
        <source>The outgoing server specified doesn&apos;t match the hostname or the one included in the certificate</source>
        <translation>指定的外連伺服器與主機名稱或證書中的名稱不相符</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="42"/>
        <source>The &quot;verify incoming certificate&quot; option is disabled, this leave you vulnerable to man in the middle attack</source>
        <translation>「驗證連入的證書」選項已被停用，這將會讓您易受中間人攻擊的影響</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="43"/>
        <source>The &quot;verify answer certificate&quot; option is disabled, this leave you vulnerable to man in the middle attack</source>
        <translation>「驗證回覆的證書」選項已被停用，這將會讓您易受中間人攻擊的影響</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="44"/>
        <source>None of your certificate provide a private key, this is required. Please select a private key or use a certificate with one built-in</source>
        <translation>您的證書未提供您的私密金鑰，這是必要的。請選取一個私密金鑰或是使用內建的證書</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="47"/>
        <source>No certificate authority is provided, it won&apos;t be possible to validate if the answer certificates are valid. Some account may also not work.</source>
        <translation>未提供證書機構，其將無法驗證回覆證書是否有效。有一些帳號可能無法運作。</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="46"/>
        <source>No certificate has been provided. This is, for now, unsupported by Ring</source>
        <translation>未提供證書。目前 Ring 不支援</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="37"/>
        <source>Your media streams are not encrypted, please enable SDES</source>
        <translation>您的媒體串流未加密，請啟用 SDES</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="50"/>
        <source>Your certificate is expired, please contact your system administrator.</source>
        <translation>您的證書已過期，請聯絡您的系統管理員。</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="51"/>
        <source>Your certificate is self signed. This break the chain of trust.</source>
        <translation>您的證書是自簽憑證。這破壞了信任鏈。</translation>
    </message>
    <message>
        <location filename="../src/tlsmethodmodel.cpp" line="64"/>
        <source>Default</source>
        <comment>Default TLS protocol version</comment>
        <translation>預設</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="517"/>
        <location filename="../src/useractionmodel.cpp" line="759"/>
        <source>Accept</source>
        <translation>接聽</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="60"/>
        <location filename="../src/useractionmodel.cpp" line="518"/>
        <location filename="../src/useractionmodel.cpp" line="771"/>
        <source>Hold</source>
        <translation>暫停通話</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="62"/>
        <source>Talking</source>
        <translation>通話中</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="64"/>
        <source>ERROR</source>
        <translation>錯誤</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="66"/>
        <source>Incoming</source>
        <translation>來電</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="68"/>
        <source>Calling</source>
        <translation>正在通話</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="70"/>
        <source>Connecting</source>
        <translation>連接中</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="72"/>
        <source>Searching</source>
        <translation>搜尋</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="74"/>
        <source>Inactive</source>
        <translation>不活躍</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="76"/>
        <location filename="../src/api/call.h" line="82"/>
        <source>Finished</source>
        <translation>已結束</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="78"/>
        <source>Timeout</source>
        <translation>逾時</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="80"/>
        <source>Peer busy</source>
        <translation>對方忙碌</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="84"/>
        <source>Communication established</source>
        <translation>已建立通訊</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="519"/>
        <source>Mute audio</source>
        <translation>靜音</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="520"/>
        <source>Mute video</source>
        <translation>視訊靜屏</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="521"/>
        <source>Server transfer</source>
        <translation>伺服器轉送</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="522"/>
        <source>Record</source>
        <translation>記錄</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="523"/>
        <location filename="../src/useractionmodel.cpp" line="789"/>
        <source>Hangup</source>
        <translation>掛斷</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="524"/>
        <source>Join</source>
        <translation>加入</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="525"/>
        <source>Add new</source>
        <translation>加入新的</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="526"/>
        <source>Toggle video</source>
        <translation>切換視訊</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="527"/>
        <source>Add a contact</source>
        <translation>新增連絡人</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="528"/>
        <source>Add to existing contact</source>
        <translation>加到既有的聯絡人</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="529"/>
        <source>Delete contact</source>
        <translation>刪除聯絡人</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="530"/>
        <source>Email contact</source>
        <translation>電子郵件聯絡人</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="531"/>
        <source>Copy contact</source>
        <translation>複製聯絡人</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="534"/>
        <source>Add phone number</source>
        <translation>新增電話號碼</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="535"/>
        <source>Call again</source>
        <translation>再次通話</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="536"/>
        <source>Edit contact details</source>
        <translation>編輯聯絡人詳細資訊</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="537"/>
        <source>Remove from history</source>
        <translation>從歷史紀錄中移除</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="786"/>
        <source>Remove</source>
        <translation>移除</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="756"/>
        <source>Call</source>
        <translation>呼叫</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="533"/>
        <source>Open chat</source>
        <translation>開啟聊天</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="768"/>
        <source>Unhold</source>
        <translation>繼續通話</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="779"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../src/localprofilecollection.cpp" line="147"/>
        <source>Local profiles</source>
        <translation>本機基本資料</translation>
    </message>
    <message>
        <location filename="../src/localprofilecollection.cpp" line="152"/>
        <source>Profile Collection</source>
        <translation>基本資料收集</translation>
    </message>
    <message>
        <location filename="../src/peerprofilecollection.cpp" line="141"/>
        <source>Peer profiles</source>
        <translation>對方的基本資料</translation>
    </message>
    <message>
        <location filename="../src/peerprofilecollection.cpp" line="146"/>
        <source>Peers Profiles Collection</source>
        <translation>對方的基本資料收集</translation>
    </message>
    <message>
        <location filename="../src/conversationmodel.cpp" line="1265"/>
        <location filename="../src/conversationmodel.cpp" line="1649"/>
        <location filename="../src/conversationmodel.cpp" line="1909"/>
        <source>Invitation received</source>
        <translation>已收到邀請</translation>
    </message>
    <message>
        <location filename="../src/conversationmodel.cpp" line="1266"/>
        <source>Contact added</source>
        <translation>已新增聯絡人</translation>
    </message>
    <message>
        <location filename="../src/conversationmodel.cpp" line="1308"/>
        <location filename="../src/conversationmodel.cpp" line="1315"/>
        <source>Invitation accepted</source>
        <translation>已接受邀請</translation>
    </message>
    <message>
        <location filename="../src/conversationmodel.cpp" line="1524"/>
        <source>ð Outgoing call</source>
        <translation>ð 撥出電話</translation>
    </message>
    <message>
        <location filename="../src/conversationmodel.cpp" line="1526"/>
        <source>ð Incoming call</source>
        <translation>ð 來電</translation>
    </message>
    <message>
        <location filename="../src/conversationmodel.cpp" line="1539"/>
        <source>ð Outgoing call - </source>
        <translation>ð 撥出電話 - </translation>
    </message>
    <message>
        <location filename="../src/conversationmodel.cpp" line="1542"/>
        <source>ð Incoming call - </source>
        <translation>ð 來電 - </translation>
    </message>
    <message>
        <location filename="../src/conversationmodel.cpp" line="1546"/>
        <source>ð½ Missed outgoing call</source>
        <translation>ð½ 未接去電</translation>
    </message>
    <message>
        <location filename="../src/conversationmodel.cpp" line="1548"/>
        <source>ð½ Missed incoming call</source>
        <translation>ð½ 未接來電</translation>
    </message>
</context>
<context>
    <name>RingDeviceModel</name>
    <message>
        <location filename="../src/ringdevicemodel.cpp" line="110"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../src/ringdevicemodel.cpp" line="112"/>
        <source>Name</source>
        <translation>名稱</translation>
    </message>
</context>
<context>
    <name>SecurityEvaluationModelPrivate</name>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="683"/>
        <source>Authority</source>
        <translation>機構</translation>
    </message>
</context>
<context>
    <name>TlsMethodModel</name>
    <message>
        <location filename="../src/tlsmethodmodel.cpp" line="101"/>
        <source>Automatic</source>
        <translation>自動</translation>
    </message>
</context>
<context>
    <name>Video::SourceModel</name>
    <message>
        <location filename="../src/video/sourcemodel.cpp" line="95"/>
        <source>NONE</source>
        <translation>無</translation>
    </message>
    <message>
        <location filename="../src/video/sourcemodel.cpp" line="101"/>
        <source>SCREEN</source>
        <translation>螢幕</translation>
    </message>
    <message>
        <location filename="../src/video/sourcemodel.cpp" line="107"/>
        <source>FILE</source>
        <translation>檔案</translation>
    </message>
</context>
<context>
    <name>lrc::ContactModelPimpl</name>
    <message>
        <location filename="../src/contactmodel.cpp" line="393"/>
        <source>Searchingâ¦</source>
        <translation>正在搜尋</translation>
    </message>
    <message>
        <location filename="../src/contactmodel.cpp" line="747"/>
        <source>Invalid ID</source>
        <translation>無效 ID</translation>
    </message>
    <message>
        <location filename="../src/contactmodel.cpp" line="750"/>
        <source>Not found</source>
        <translation>找不到</translation>
    </message>
    <message>
        <location filename="../src/contactmodel.cpp" line="753"/>
        <source>Couldn&apos;t lookupâ¦</source>
        <translation>無法查詢！</translation>
    </message>
</context>
<context>
    <name>lrc::api::ContactModel</name>
    <message>
        <location filename="../src/contactmodel.cpp" line="364"/>
        <source>Bad URI scheme</source>
        <translation>錯誤的 URI 結構</translation>
    </message>
</context>
<context>
    <name>media::RecordingModel</name>
    <message>
        <location filename="../src/media/recordingmodel.cpp" line="189"/>
        <source>Recordings</source>
        <translation>記錄</translation>
    </message>
    <message>
        <location filename="../src/media/recordingmodel.cpp" line="259"/>
        <source>Text messages</source>
        <translation>文字訊息</translation>
    </message>
    <message>
        <location filename="../src/media/recordingmodel.cpp" line="264"/>
        <source>Audio/Video</source>
        <translation>音訊／視訊</translation>
    </message>
</context>
</TS>