<?xml version="1.0" ?><!DOCTYPE TS><TS language="sv" sourcelanguage="en" version="2.0">
<context>
    <name>Account</name>
    <message>
        <location filename="../src/account.cpp" line="307"/>
        <source>Ready</source>
        <extracomment>Account state</extracomment>
        <translation>Klar</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="309"/>
        <source>Registered</source>
        <extracomment>Account state</extracomment>
        <translation>Registrerad</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="313"/>
        <source>Initializing</source>
        <extracomment>Account state</extracomment>
        <translation>Startar</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="317"/>
        <source>Error</source>
        <extracomment>Account state</extracomment>
        <translation>Fel</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="321"/>
        <source>Network unreachable</source>
        <extracomment>Account state</extracomment>
        <translation>Nätverket onåbart</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="323"/>
        <source>Host unreachable</source>
        <extracomment>Account state</extracomment>
        <translation>Värdenhet onåbar</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="311"/>
        <source>Not registered</source>
        <extracomment>Account state</extracomment>
        <translation>Inte registrerad</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="315"/>
        <source>Tryingâ¦</source>
        <extracomment>Account state</extracomment>
        <translation>Försöker!</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="319"/>
        <source>Authentication failed</source>
        <extracomment>Account state</extracomment>
        <translation>Autentisering misslyckades</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="325"/>
        <source>STUN configuration error</source>
        <extracomment>Account state</extracomment>
        <translation>STUN konfigurationsfel</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="327"/>
        <source>STUN server invalid</source>
        <extracomment>Account state</extracomment>
        <translation>STUN-server ogiltig</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="329"/>
        <source>Service unavailable</source>
        <extracomment>Account state</extracomment>
        <translation>Tjänst ej tillgänglig</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="331"/>
        <source>Unacceptable</source>
        <extracomment>Account state</extracomment>
        <translation>Oacceptabel</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="333"/>
        <source>Invalid</source>
        <extracomment>Account state</extracomment>
        <translation>Ogiltig</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="335"/>
        <source>Request timeout</source>
        <extracomment>Account state</extracomment>
        <translation>Timeout för begäran</translation>
    </message>
</context>
<context>
    <name>AccountChecksModel</name>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="438"/>
        <source>Configuration</source>
        <translation>Konfiguration</translation>
    </message>
</context>
<context>
    <name>Call</name>
    <message>
        <location filename="../src/call.cpp" line="719"/>
        <source>New</source>
        <extracomment>Call state</extracomment>
        <translation>Nytt</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="722"/>
        <source>Ringing</source>
        <extracomment>Call state</extracomment>
        <translation>Ringer</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="725"/>
        <source>Calling</source>
        <extracomment>Call state</extracomment>
        <translation>Ringer</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="728"/>
        <source>Talking</source>
        <extracomment>Call state</extracomment>
        <translation>Talar</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="731"/>
        <source>Dialing</source>
        <extracomment>Call state</extracomment>
        <translation>Ringer</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="734"/>
        <source>Hold</source>
        <extracomment>Call state</extracomment>
        <translation>Parkera</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="737"/>
        <source>Failed</source>
        <extracomment>Call state</extracomment>
        <translation>Misslyckades</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="740"/>
        <source>Busy</source>
        <extracomment>Call state</extracomment>
        <translation>Upptagen</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="743"/>
        <source>Transfer</source>
        <extracomment>Call state</extracomment>
        <translation>Överför</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="746"/>
        <source>Transfer hold</source>
        <extracomment>Call state</extracomment>
        <translation>Överföringspaus</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="749"/>
        <source>Over</source>
        <extracomment>Call state</extracomment>
        <translation>Över</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="752"/>
        <source>Error</source>
        <extracomment>Call state</extracomment>
        <translation>Fel</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="755"/>
        <location filename="../src/call.cpp" line="860"/>
        <source>Conference</source>
        <extracomment>Call state</extracomment>
        <translation>Konferens</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="758"/>
        <source>Conference (hold)</source>
        <extracomment>Call state</extracomment>
        <translation>Konferens (parkerad)</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="761"/>
        <source>ERROR</source>
        <extracomment>Call state</extracomment>
        <translation>FEL</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="764"/>
        <source>Searching for</source>
        <extracomment>Call state</extracomment>
        <translation>Söker efter</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="767"/>
        <source>Aborted</source>
        <extracomment>Call state</extracomment>
        <translation>Avbruten</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="770"/>
        <source>Communication established</source>
        <extracomment>Call state</extracomment>
        <translation>Kommunikation etablerad</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="864"/>
        <source>Unknown</source>
        <translation>Okänd</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="2231"/>
        <source>Account: </source>
        <translation>Konto:</translation>
    </message>
</context>
<context>
    <name>CallModel</name>
    <message>
        <location filename="../src/callmodel.cpp" line="898"/>
        <source>Calls</source>
        <translation>Samtal</translation>
    </message>
</context>
<context>
    <name>CallModelPrivate</name>
    <message>
        <location filename="../src/callmodel.cpp" line="533"/>
        <location filename="../src/callmodel.cpp" line="558"/>
        <source>Invalid account</source>
        <translation>Ogiltigt konto</translation>
    </message>
</context>
<context>
    <name>CallPrivate</name>
    <message>
        <location filename="../src/call.cpp" line="1795"/>
        <source>Aborted</source>
        <translation>Avbruten</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="1816"/>
        <source>No account registered!</source>
        <translation>Inget konto registrerat!</translation>
    </message>
</context>
<context>
    <name>CategorizedBookmarkModel</name>
    <message>
        <location filename="../src/categorizedbookmarkmodel.cpp" line="181"/>
        <source>Most popular</source>
        <extracomment>Most popular contacts</extracomment>
        <translation>Mest populär</translation>
    </message>
    <message>
        <location filename="../src/categorizedbookmarkmodel.cpp" line="299"/>
        <source>Contacts</source>
        <translation>Kontakter</translation>
    </message>
</context>
<context>
    <name>CategorizedContactModel</name>
    <message>
        <location filename="../src/categorizedcontactmodel.cpp" line="401"/>
        <source>Contacts</source>
        <translation>Kontakter</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="129"/>
        <source>Empty</source>
        <translation>Tom</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="135"/>
        <location filename="../src/private/sortproxies.cpp" line="153"/>
        <source>Unknown</source>
        <translation>Okänd</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="141"/>
        <source>Never</source>
        <translation>Aldrig</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="147"/>
        <source>Other</source>
        <translation>Annan</translation>
    </message>
</context>
<context>
    <name>CategorizedHistoryModel</name>
    <message>
        <location filename="../src/categorizedhistorymodel.cpp" line="412"/>
        <source>History</source>
        <translation>Historik</translation>
    </message>
</context>
<context>
    <name>ChainOfTrustModel</name>
    <message>
        <location filename="../src/chainoftrustmodel.cpp" line="173"/>
        <source>Chain of trust</source>
        <translation>Förtroendekedja</translation>
    </message>
</context>
<context>
    <name>CollectionModel</name>
    <message>
        <location filename="../src/collectionmodel.cpp" line="279"/>
        <source>Name</source>
        <translation>Namn</translation>
    </message>
</context>
<context>
    <name>ContactSortingCategoryModel</name>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="49"/>
        <source>Name</source>
        <translation>Namn</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="50"/>
        <source>Organisation</source>
        <translation>Organisation</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="51"/>
        <source>Recently used</source>
        <translation>Nyligen använd</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="52"/>
        <source>Group</source>
        <translation>Grupp</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="53"/>
        <source>Department</source>
        <translation>Avdelning</translation>
    </message>
</context>
<context>
    <name>HistorySortingCategoryModel</name>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="57"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="58"/>
        <source>Name</source>
        <translation>Namn</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="59"/>
        <source>Popularity</source>
        <translation>Popularitet</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="60"/>
        <source>Duration</source>
        <translation>Varaktighet</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="61"/>
        <source>Total time</source>
        <translation>Sammanlagd tid</translation>
    </message>
</context>
<context>
    <name>HistoryTimeCategoryModel</name>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="50"/>
        <source>Today</source>
        <translation>I dag</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="51"/>
        <source>Yesterday</source>
        <translation>I går</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="58"/>
        <source>Two weeks ago</source>
        <translation>Två veckor sedan</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="59"/>
        <source>Three weeks ago</source>
        <translation>Tre veckor sedan</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="57"/>
        <source>A week ago</source>
        <translation>En vecka sedan</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="60"/>
        <source>A month ago</source>
        <translation>En månad sedan</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="61"/>
        <source>Two months ago</source>
        <translation>2 månader sedan</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="62"/>
        <source>Three months ago</source>
        <translation>3 månader sedan</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="63"/>
        <source>Four months ago</source>
        <translation>4 månader sedan</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="64"/>
        <source>Five months ago</source>
        <translation>5 månader sedan</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="65"/>
        <source>Six months ago</source>
        <translation>6 månader sedan</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="66"/>
        <source>Seven months ago</source>
        <translation>7 månader sedan</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="67"/>
        <source>Eight months ago</source>
        <translation>8 månader sedan</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="68"/>
        <source>Nine months ago</source>
        <translation>9 månader sedan</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="69"/>
        <source>Ten months ago</source>
        <translation>10 månader sedan</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="70"/>
        <source>Eleven months ago</source>
        <translation>11 månader sedan</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="71"/>
        <source>Twelve months ago</source>
        <translation>12 månader sedan</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="72"/>
        <source>A year ago</source>
        <translation>Ett år sedan</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="73"/>
        <source>Very long time ago</source>
        <translation>Mycket länge sedan</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="74"/>
        <source>Never</source>
        <translation>Aldrig</translation>
    </message>
</context>
<context>
    <name>InstantMessagingModel</name>
    <message>
        <location filename="../src/media/textrecording.cpp" line="816"/>
        <source>Me</source>
        <translation>Jag</translation>
    </message>
</context>
<context>
    <name>MacroModel</name>
    <message>
        <location filename="../src/macromodel.cpp" line="157"/>
        <source>Macros</source>
        <translation>Macron</translation>
    </message>
    <message>
        <location filename="../src/macromodel.cpp" line="263"/>
        <source>New</source>
        <translation>Nytt</translation>
    </message>
    <message>
        <location filename="../src/macromodel.cpp" line="264"/>
        <source>Other</source>
        <translation>Annat</translation>
    </message>
</context>
<context>
    <name>MacroModelPrivate</name>
    <message>
        <location filename="../src/macromodel.cpp" line="77"/>
        <source>Other</source>
        <translation>Annat</translation>
    </message>
</context>
<context>
    <name>NumberCategoryModel</name>
    <message>
        <location filename="../src/numbercategorymodel.cpp" line="56"/>
        <source>Uncategorized</source>
        <translation>Okategoriserad</translation>
    </message>
</context>
<context>
    <name>PersonModel</name>
    <message>
        <location filename="../src/personmodel.cpp" line="170"/>
        <source>Persons</source>
        <translation>Personer</translation>
    </message>
</context>
<context>
    <name>PhoneDirectoryModel</name>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="234"/>
        <source>This account does not support presence tracking</source>
        <translation>Detta konto stöder inte närvarospårning</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="237"/>
        <source>No associated account</source>
        <translation>Inget associerat konto</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="305"/>
        <source>URI</source>
        <translation>URI</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="305"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="305"/>
        <source>Person</source>
        <translation>Person</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="305"/>
        <source>Account</source>
        <translation>Konto</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="305"/>
        <source>State</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="305"/>
        <source>Call count</source>
        <translation>Samtalsräknare</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="305"/>
        <source>Week count</source>
        <translation>Antal veckor</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="306"/>
        <source>Trimester count</source>
        <translation>Antal fyramånadersperioder</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="306"/>
        <source>Have Called</source>
        <translation>Har ringt</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="306"/>
        <source>Last used</source>
        <translation>Senast använd</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="306"/>
        <source>Name_count</source>
        <translation>Antal_namn</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="306"/>
        <source>Total (in seconds)</source>
        <translation>Totalt (i sekunder)</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="306"/>
        <source>Popularity_index</source>
        <translation>Popularitetsindex</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="307"/>
        <source>Bookmarked</source>
        <translation>Bokmärkt</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="307"/>
        <source>Tracked</source>
        <translation>Spårad</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="307"/>
        <source>Present</source>
        <translation>Närvarande</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="307"/>
        <source>Presence message</source>
        <translation>Närvaromeddelande</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="307"/>
        <source>UID</source>
        <translation>UID</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="307"/>
        <source>Has certificate</source>
        <translation>Har certifikat</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="307"/>
        <source>Registered name</source>
        <translation>Registrerat namn</translation>
    </message>
</context>
<context>
    <name>PresenceStatusModel</name>
    <message>
        <location filename="../src/presencestatusmodel.cpp" line="197"/>
        <source>Name</source>
        <translation>Namn</translation>
    </message>
    <message>
        <location filename="../src/presencestatusmodel.cpp" line="197"/>
        <source>Message</source>
        <translation>Meddelande</translation>
    </message>
    <message>
        <location filename="../src/presencestatusmodel.cpp" line="197"/>
        <source>Color</source>
        <translation>Färg</translation>
    </message>
    <message>
        <location filename="../src/presencestatusmodel.cpp" line="197"/>
        <source>Present</source>
        <translation>Närvarande</translation>
    </message>
    <message>
        <location filename="../src/presencestatusmodel.cpp" line="197"/>
        <source>Default</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../src/presencestatusmodel.cpp" line="308"/>
        <location filename="../src/presencestatusmodel.cpp" line="361"/>
        <source>Custom</source>
        <translation>Anpassad</translation>
    </message>
    <message>
        <location filename="../src/presencestatusmodel.cpp" line="308"/>
        <location filename="../src/presencestatusmodel.cpp" line="310"/>
        <location filename="../src/presencestatusmodel.cpp" line="354"/>
        <location filename="../src/presencestatusmodel.cpp" line="361"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
</context>
<context>
    <name>ProfileModel</name>
    <message>
        <location filename="../src/profilemodel.cpp" line="637"/>
        <source>Profiles</source>
        <translation>Profiler</translation>
    </message>
</context>
<context>
    <name>ProfileModelPrivate</name>
    <message>
        <location filename="../src/profilemodel.cpp" line="751"/>
        <source>New profile</source>
        <translation>Ny profil</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/certificate.cpp" line="42"/>
        <source>Has a private key</source>
        <translation>Har en privat nyckel</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="43"/>
        <source>Is not expired</source>
        <translation>Har inte löpt ut</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="44"/>
        <source>Has strong signing</source>
        <translation>Har stark signering</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="45"/>
        <source>Is not self signed</source>
        <translation>Är inte självsignerad</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="46"/>
        <source>Have a matching key pair</source>
        <translation>Har ett matchande nyckelpar</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="47"/>
        <source>Has the right private key file permissions</source>
        <translation>Har de rätta behörigheterna för privat nyckel</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="48"/>
        <source>Has the right public key file permissions</source>
        <translation>Har de rätta behörigheterna för offentlig nyckel</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="49"/>
        <source>Has the right private key directory permissions</source>
        <translation>Har de rätta behörigheterna för privat nyckelkatalog</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="50"/>
        <source>Has the right public key directory permissions</source>
        <translation>Har de rätta behörigheterna för offentlig nyckelkatalog</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="51"/>
        <source>Has the right private key directory location</source>
        <translation>Har den rätta katalogplatsen för privat nyckel</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="52"/>
        <source>Has the right public key directory location</source>
        <translation>Har den rätta katalogplatsen för offentlig nyckel</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="53"/>
        <source>Has the right private key SELinux attributes</source>
        <translation>Har de rätta SELinux-attributen för privat nyckel</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="54"/>
        <source>Has the right public key SELinux attributes</source>
        <translation>Har de rätta SELinux-attributen för offentlig nyckel</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="55"/>
        <source>The certificate file exist and is readable</source>
        <translation>Certifikatfilen finns och är läsbar</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="56"/>
        <source>The file is a valid certificate</source>
        <translation>Den här filen är ett giltigt certifikat</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="57"/>
        <source>The certificate has a valid authority</source>
        <translation>Certifikatet har en giltig utfärdare</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="58"/>
        <source>The certificate has a known authority</source>
        <translation>Certifikatet har en känd utfärdare</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="59"/>
        <source>The certificate is not revoked</source>
        <translation>Certifikatet är inte återkallat</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="60"/>
        <source>The certificate authority match</source>
        <translation>Certifikatutfärdaren stämmer</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="61"/>
        <source>The certificate has the expected owner</source>
        <translation>Certifikatet har förväntad ägare</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="62"/>
        <source>The certificate is within its active period</source>
        <translation>Certifikatet är inom sin aktiva period</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="90"/>
        <source>Expiration date</source>
        <translation>Utgångsdatum</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="91"/>
        <source>Activation date</source>
        <translation>Aktiveringsdatum</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="92"/>
        <source>Require a private key password</source>
        <translation>Kräv lösenord för privat nyckel</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="93"/>
        <source>Public signature</source>
        <translation>Offentlig signatur</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="94"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="95"/>
        <source>Serial number</source>
        <translation>Serienummer</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="96"/>
        <source>Issuer</source>
        <translation>Utfärdare</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="97"/>
        <source>Subject key algorithm</source>
        <translation>Angående nyckelalgoritm</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="98"/>
        <source>Common name (CN)</source>
        <translation>Unikt namn (CN)</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="99"/>
        <source>Name (N)</source>
        <translation>Namn (N)</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="100"/>
        <source>Organization (O)</source>
        <translation>Organisation (O)</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="101"/>
        <source>Signature algorithm</source>
        <translation>Signaturalgoritm</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="102"/>
        <source>Md5 fingerprint</source>
        <translation>MD5-fingeravtryck</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="103"/>
        <source>Sha1 fingerprint</source>
        <translation>SHA1-fingeravtryck</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="104"/>
        <source>Public key ID</source>
        <translation>Offentlig nyckel-ID</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="105"/>
        <source>Issuer domain name</source>
        <translation>Utfärdarens domännamn</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="106"/>
        <source>Next expected update</source>
        <translation>Nästa förväntade uppdatering</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="107"/>
        <source>Outgoing server</source>
        <translation>Utgående server</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="183"/>
        <source>Local certificate store</source>
        <translation>Lokal certifikatlagring</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="267"/>
        <location filename="../src/localprofilecollection.cpp" line="220"/>
        <source>Default</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="267"/>
        <source>Certificate not associated with a group</source>
        <translation>Certifikatet inte associerat med någon grupp</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="397"/>
        <source>A certificate</source>
        <translation>Ett certifikat</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="397"/>
        <source>An organisation</source>
        <translation>En organisation</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="420"/>
        <source>Details</source>
        <translation>Detaljer</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="420"/>
        <source>The content of the certificate</source>
        <translation>Certifikatets innehåll</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="421"/>
        <source>Checks</source>
        <translation>Kontroller</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="421"/>
        <source>Various security related information</source>
        <translation>Diverse säkerhetsrelaterad information</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="610"/>
        <source>Header</source>
        <translation>Header</translation>
    </message>
    <message>
        <location filename="../src/daemoncertificatecollection.cpp" line="148"/>
        <source>Daemon certificate store</source>
        <translation>Demoncertifikatlagring</translation>
    </message>
    <message>
        <location filename="../src/daemoncertificatecollection.cpp" line="152"/>
        <source>%1 banned list</source>
        <extracomment>The list of banned certificates for this account</extracomment>
        <translation>%1 bannlystlista</translation>
    </message>
    <message>
        <location filename="../src/daemoncertificatecollection.cpp" line="155"/>
        <source>%1 allowed list</source>
        <extracomment>The list of allowed certificates for this account</extracomment>
        <translation>%1 tillåtenlista</translation>
    </message>
    <message>
        <location filename="../src/daemoncertificatecollection.cpp" line="160"/>
        <location filename="../src/foldercertificatecollection.cpp" line="198"/>
        <source>Certificate</source>
        <translation>Certifikat</translation>
    </message>
    <message>
        <location filename="../src/extensions/presencecollectionextension.cpp" line="38"/>
        <source>Presence tracking</source>
        <translation>Närvarospårning</translation>
    </message>
    <message>
        <location filename="../src/extensions/securityevaluationextension.cpp" line="63"/>
        <source>Security evaluation</source>
        <translation>Säkerhetsutvärdering</translation>
    </message>
    <message>
        <location filename="../src/fallbackpersoncollection.cpp" line="194"/>
        <source>Contact</source>
        <translation>Kontakt</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="532"/>
        <source>Bookmark</source>
        <translation>Bokmärke</translation>
    </message>
    <message>
        <location filename="../src/localhistorycollection.cpp" line="195"/>
        <source>Local history</source>
        <translation>Lokal historik</translation>
    </message>
    <message>
        <location filename="../src/localhistorycollection.cpp" line="200"/>
        <source>History</source>
        <translation>Historik</translation>
    </message>
    <message>
        <location filename="../src/localrecordingcollection.cpp" line="101"/>
        <source>Local recordings</source>
        <translation>Lokala inspelningar</translation>
    </message>
    <message>
        <location filename="../src/localrecordingcollection.cpp" line="106"/>
        <location filename="../src/localtextrecordingcollection.cpp" line="170"/>
        <source>Recording</source>
        <translation>Inspelning</translation>
    </message>
    <message>
        <location filename="../src/localringtonecollection.cpp" line="220"/>
        <source>Local ringtones</source>
        <translation>Lokala ringsignaler</translation>
    </message>
    <message>
        <location filename="../src/localringtonecollection.cpp" line="225"/>
        <source>Ringtone</source>
        <translation>Ringsignal</translation>
    </message>
    <message>
        <location filename="../src/localtextrecordingcollection.cpp" line="165"/>
        <source>Local text recordings</source>
        <translation>Lokala textinspelningar</translation>
    </message>
    <message>
        <location filename="../src/numbercategory.cpp" line="72"/>
        <source>Phone number types</source>
        <translation>Telefonnummertyper</translation>
    </message>
    <message>
        <location filename="../src/numbercategorymodel.cpp" line="186"/>
        <source>Other</source>
        <translation>Annat</translation>
    </message>
    <message>
        <location filename="../src/protocolmodel.cpp" line="50"/>
        <source>Ring Account</source>
        <translation>Ring-konto</translation>
    </message>
    <message>
        <location filename="../src/protocolmodel.cpp" line="51"/>
        <source>SIP Account</source>
        <translation>SIP-konto</translation>
    </message>
    <message>
        <location filename="../src/qtwrapper/callmanager_wrap.h" line="315"/>
        <source>Me</source>
        <translation>Jag</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="38"/>
        <source>TLS is disabled, the negotiation won&apos;t be encrypted. Your communication will be vulnerable to snooping</source>
        <translation>TLS är inaktiverat, förhandlingen krypteras inte. Din kommunikation kommer att vara sårbar för avlyssning</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="40"/>
        <source>Your certificate and authority don&apos;t match, if your certificate require an authority, it won&apos;t work</source>
        <translation>Ditt certifikat och utfärdare stämmer inte. Om ditt certifikat kräver en utfärdare, kommer det inte att fungera</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="41"/>
        <source>The outgoing server specified doesn&apos;t match the hostname or the one included in the certificate</source>
        <translation>Den specificerade servern stämmer inte med värdnamnet ellerdet  som finns i certifikatet</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="42"/>
        <source>The &quot;verify incoming certificate&quot; option is disabled, this leave you vulnerable to man in the middle attack</source>
        <translation>Alternativet &quot;Verifiera inkommande certifikat&quot; är inaktiverat. Detta gör dig sårbar för så kallade &quot;man-in-the-middle-attacker&quot;</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="43"/>
        <source>The &quot;verify answer certificate&quot; option is disabled, this leave you vulnerable to man in the middle attack</source>
        <translation>Alternativet &quot;Verifiera svarscertifikat&quot; är inaktiverat. Detta gör dig sårbar för så kallade &quot;man-in-the-middle-attacker&quot;</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="44"/>
        <source>None of your certificate provide a private key, this is required. Please select a private key or use a certificate with one built-in</source>
        <translation>Inget av dina certifikat tillhandahåller en privat nyckel, vilket krävs. Välj en privat nyckel eller använd ett certifikat med inbyggd privat nyckel</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="47"/>
        <source>No certificate authority is provided, it won&apos;t be possible to validate if the answer certificates are valid. Some account may also not work.</source>
        <translation>Ingen certifikatutfärdare tillhandahålls. Validering av svarscertifikatets giltighet kommer inte att vara möjlig.Vissa konton kanske inte heller fungerar.</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="46"/>
        <source>No certificate has been provided. This is, for now, unsupported by Ring</source>
        <translation>Inget certifikat tillhandahålls. Detta stöds för närvarande inte av Ring</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="37"/>
        <source>Your media streams are not encrypted, please enable SDES</source>
        <translation>Dina mediaströmmar är inte krypterade, aktivera SDES</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="50"/>
        <source>Your certificate is expired, please contact your system administrator.</source>
        <translation>Ditt certifikat har löpt ut. Kontakta din systemadministratör.</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="51"/>
        <source>Your certificate is self signed. This break the chain of trust.</source>
        <translation>Ditt certifikat är självsignerat. Detta bryter förtroendekedjan.</translation>
    </message>
    <message>
        <location filename="../src/tlsmethodmodel.cpp" line="64"/>
        <source>Default</source>
        <comment>Default TLS protocol version</comment>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="517"/>
        <location filename="../src/useractionmodel.cpp" line="759"/>
        <source>Accept</source>
        <translation>Acceptera</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="60"/>
        <location filename="../src/useractionmodel.cpp" line="518"/>
        <location filename="../src/useractionmodel.cpp" line="771"/>
        <source>Hold</source>
        <translation>Parkera</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="62"/>
        <source>Talking</source>
        <translation>Talar</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="64"/>
        <source>ERROR</source>
        <translation>FEL</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="66"/>
        <source>Incoming</source>
        <translation>Inkommande</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="68"/>
        <source>Calling</source>
        <translation>Ringer</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="70"/>
        <source>Connecting</source>
        <translation>Ansluter</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="72"/>
        <source>Searching</source>
        <translation>Söker</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="74"/>
        <source>Inactive</source>
        <translation>Inaktiv</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="76"/>
        <location filename="../src/api/call.h" line="82"/>
        <source>Finished</source>
        <translation>Slutförd</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="78"/>
        <source>Timeout</source>
        <translation>Tidsgräns uppnådd</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="80"/>
        <source>Peer busy</source>
        <translation>Användare upptagen</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="84"/>
        <source>Communication established</source>
        <translation>Kommunikation etablerad</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="519"/>
        <source>Mute audio</source>
        <translation>Pausa ljud</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="520"/>
        <source>Mute video</source>
        <translation>Pausa video</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="521"/>
        <source>Server transfer</source>
        <translation>Serveröverföring</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="522"/>
        <source>Record</source>
        <translation>Spela in</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="523"/>
        <location filename="../src/useractionmodel.cpp" line="789"/>
        <source>Hangup</source>
        <translation>Lägg på</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="524"/>
        <source>Join</source>
        <translation>Anslut</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="525"/>
        <source>Add new</source>
        <translation>Lägg till ny</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="526"/>
        <source>Toggle video</source>
        <translation>Växla videostatus</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="527"/>
        <source>Add a contact</source>
        <translation>Lägg till en kontakt</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="528"/>
        <source>Add to existing contact</source>
        <translation>Lägg till i befintlig kontakt</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="529"/>
        <source>Delete contact</source>
        <translation>Ta bort kontakt</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="530"/>
        <source>Email contact</source>
        <translation>E-postkontakt</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="531"/>
        <source>Copy contact</source>
        <translation>Kopiera kontakt</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="534"/>
        <source>Add phone number</source>
        <translation>Lägg till telefonnummer</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="535"/>
        <source>Call again</source>
        <translation>Ring upp igen</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="536"/>
        <source>Edit contact details</source>
        <translation>Redigera kontaktdetaljer</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="537"/>
        <source>Remove from history</source>
        <translation>Ta bort från historiken</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="786"/>
        <source>Remove</source>
        <translation>Ta bort</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="756"/>
        <source>Call</source>
        <translation>Ring upp</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="533"/>
        <source>Open chat</source>
        <translation>Öppna chatt</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="768"/>
        <source>Unhold</source>
        <translation>Återuppta</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="779"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="../src/localprofilecollection.cpp" line="147"/>
        <source>Local profiles</source>
        <translation>Lokala profiler</translation>
    </message>
    <message>
        <location filename="../src/localprofilecollection.cpp" line="152"/>
        <source>Profile Collection</source>
        <translation>Profilsamling</translation>
    </message>
    <message>
        <location filename="../src/peerprofilecollection.cpp" line="141"/>
        <source>Peer profiles</source>
        <translation>Deltagarprofiler</translation>
    </message>
    <message>
        <location filename="../src/peerprofilecollection.cpp" line="146"/>
        <source>Peers Profiles Collection</source>
        <translation>Deltagarprofilsamling</translation>
    </message>
    <message>
        <location filename="../src/conversationmodel.cpp" line="1265"/>
        <location filename="../src/conversationmodel.cpp" line="1649"/>
        <location filename="../src/conversationmodel.cpp" line="1909"/>
        <source>Invitation received</source>
        <translation>Inbjudan mottagen</translation>
    </message>
    <message>
        <location filename="../src/conversationmodel.cpp" line="1266"/>
        <source>Contact added</source>
        <translation>Kontakt tillagt</translation>
    </message>
    <message>
        <location filename="../src/conversationmodel.cpp" line="1308"/>
        <location filename="../src/conversationmodel.cpp" line="1315"/>
        <source>Invitation accepted</source>
        <translation>Inbjudan accepterad</translation>
    </message>
    <message>
        <location filename="../src/conversationmodel.cpp" line="1524"/>
        <source>ð Outgoing call</source>
        <translation>ð Utgående samtal</translation>
    </message>
    <message>
        <location filename="../src/conversationmodel.cpp" line="1526"/>
        <source>ð Incoming call</source>
        <translation>ð Inkommande samtal</translation>
    </message>
    <message>
        <location filename="../src/conversationmodel.cpp" line="1539"/>
        <source>ð Outgoing call - </source>
        <translation>ð Utgående samtal - </translation>
    </message>
    <message>
        <location filename="../src/conversationmodel.cpp" line="1542"/>
        <source>ð Incoming call - </source>
        <translation>ð Inkommande samtal - </translation>
    </message>
    <message>
        <location filename="../src/conversationmodel.cpp" line="1546"/>
        <source>ð½ Missed outgoing call</source>
        <translation>ð½ Missat utgående samtal</translation>
    </message>
    <message>
        <location filename="../src/conversationmodel.cpp" line="1548"/>
        <source>ð½ Missed incoming call</source>
        <translation>ð½ Missat inkommande samtal</translation>
    </message>
</context>
<context>
    <name>RingDeviceModel</name>
    <message>
        <location filename="../src/ringdevicemodel.cpp" line="110"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../src/ringdevicemodel.cpp" line="112"/>
        <source>Name</source>
        <translation>Namn</translation>
    </message>
</context>
<context>
    <name>SecurityEvaluationModelPrivate</name>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="683"/>
        <source>Authority</source>
        <translation>Utfärdare</translation>
    </message>
</context>
<context>
    <name>TlsMethodModel</name>
    <message>
        <location filename="../src/tlsmethodmodel.cpp" line="101"/>
        <source>Automatic</source>
        <translation>Automatisk</translation>
    </message>
</context>
<context>
    <name>Video::SourceModel</name>
    <message>
        <location filename="../src/video/sourcemodel.cpp" line="95"/>
        <source>NONE</source>
        <translation>INGEN</translation>
    </message>
    <message>
        <location filename="../src/video/sourcemodel.cpp" line="101"/>
        <source>SCREEN</source>
        <translation>SKÄRM</translation>
    </message>
    <message>
        <location filename="../src/video/sourcemodel.cpp" line="107"/>
        <source>FILE</source>
        <translation>FIL</translation>
    </message>
</context>
<context>
    <name>lrc::ContactModelPimpl</name>
    <message>
        <location filename="../src/contactmodel.cpp" line="393"/>
        <source>Searchingâ¦</source>
        <translation>Söker</translation>
    </message>
    <message>
        <location filename="../src/contactmodel.cpp" line="747"/>
        <source>Invalid ID</source>
        <translation>Ogiltigt ID</translation>
    </message>
    <message>
        <location filename="../src/contactmodel.cpp" line="750"/>
        <source>Not found</source>
        <translation>Kunde inte hittas</translation>
    </message>
    <message>
        <location filename="../src/contactmodel.cpp" line="753"/>
        <source>Couldn&apos;t lookupâ¦</source>
        <translation>Kunde inte slå upp</translation>
    </message>
</context>
<context>
    <name>lrc::api::ContactModel</name>
    <message>
        <location filename="../src/contactmodel.cpp" line="364"/>
        <source>Bad URI scheme</source>
        <translation>Felaktigt URI-schema</translation>
    </message>
</context>
<context>
    <name>media::RecordingModel</name>
    <message>
        <location filename="../src/media/recordingmodel.cpp" line="189"/>
        <source>Recordings</source>
        <translation>Inspelningar</translation>
    </message>
    <message>
        <location filename="../src/media/recordingmodel.cpp" line="259"/>
        <source>Text messages</source>
        <translation>Textmeddelanden</translation>
    </message>
    <message>
        <location filename="../src/media/recordingmodel.cpp" line="264"/>
        <source>Audio/Video</source>
        <translation>Ljud/Video</translation>
    </message>
</context>
</TS>