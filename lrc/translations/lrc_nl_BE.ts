<?xml version="1.0" ?><!DOCTYPE TS><TS language="nl_BE" sourcelanguage="en" version="2.0">
<context>
    <name>Account</name>
    <message>
        <location filename="../src/account.cpp" line="307"/>
        <source>Ready</source>
        <extracomment>Account state</extracomment>
        <translation>Klaar</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="309"/>
        <source>Registered</source>
        <extracomment>Account state</extracomment>
        <translation>Geregistreerd</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="313"/>
        <source>Initializing</source>
        <extracomment>Account state</extracomment>
        <translation>Bezig met initialiseren</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="317"/>
        <source>Error</source>
        <extracomment>Account state</extracomment>
        <translation>Fout</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="321"/>
        <source>Network unreachable</source>
        <extracomment>Account state</extracomment>
        <translation>Netwerk onbereikbaar</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="323"/>
        <source>Host unreachable</source>
        <extracomment>Account state</extracomment>
        <translation>Host onbereikbaar</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="311"/>
        <source>Not registered</source>
        <extracomment>Account state</extracomment>
        <translation>Niet geregistreerd</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="315"/>
        <source>Tryingâ¦</source>
        <extracomment>Account state</extracomment>
        <translation>Proberen</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="319"/>
        <source>Authentication failed</source>
        <extracomment>Account state</extracomment>
        <translation>Authenticatie mislukt</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="325"/>
        <source>STUN configuration error</source>
        <extracomment>Account state</extracomment>
        <translation>STUN-configuratiefout</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="327"/>
        <source>STUN server invalid</source>
        <extracomment>Account state</extracomment>
        <translation>STUN-server ongeldig</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="329"/>
        <source>Service unavailable</source>
        <extracomment>Account state</extracomment>
        <translation>Dienst nie beschikbaar</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="331"/>
        <source>Unacceptable</source>
        <extracomment>Account state</extracomment>
        <translation>Onaanvaardbaar</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="333"/>
        <source>Invalid</source>
        <extracomment>Account state</extracomment>
        <translation>Ongeldig</translation>
    </message>
    <message>
        <location filename="../src/account.cpp" line="335"/>
        <source>Request timeout</source>
        <extracomment>Account state</extracomment>
        <translation>Aanvraag time-out</translation>
    </message>
</context>
<context>
    <name>AccountChecksModel</name>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="438"/>
        <source>Configuration</source>
        <translation>Configuratie</translation>
    </message>
</context>
<context>
    <name>Call</name>
    <message>
        <location filename="../src/call.cpp" line="719"/>
        <source>New</source>
        <extracomment>Call state</extracomment>
        <translation>Nieuw</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="722"/>
        <source>Ringing</source>
        <extracomment>Call state</extracomment>
        <translation>Gaad over</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="725"/>
        <source>Calling</source>
        <extracomment>Call state</extracomment>
        <translation>Bellen</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="728"/>
        <source>Talking</source>
        <extracomment>Call state</extracomment>
        <translation>Praten</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="731"/>
        <source>Dialing</source>
        <extracomment>Call state</extracomment>
        <translation>Kiezen</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="734"/>
        <source>Hold</source>
        <extracomment>Call state</extracomment>
        <translation>In wacht zetten</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="737"/>
        <source>Failed</source>
        <extracomment>Call state</extracomment>
        <translation>Mislukt</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="740"/>
        <source>Busy</source>
        <extracomment>Call state</extracomment>
        <translation>Bezig</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="743"/>
        <source>Transfer</source>
        <extracomment>Call state</extracomment>
        <translation>Overdracht</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="746"/>
        <source>Transfer hold</source>
        <extracomment>Call state</extracomment>
        <translation>In wacht door overdracht</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="749"/>
        <source>Over</source>
        <extracomment>Call state</extracomment>
        <translation>Opg’hangen</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="752"/>
        <source>Error</source>
        <extracomment>Call state</extracomment>
        <translation>Fout</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="755"/>
        <location filename="../src/call.cpp" line="860"/>
        <source>Conference</source>
        <extracomment>Call state</extracomment>
        <translation>Vergadering</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="758"/>
        <source>Conference (hold)</source>
        <extracomment>Call state</extracomment>
        <translation>Vergadering (wacht)</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="761"/>
        <source>ERROR</source>
        <extracomment>Call state</extracomment>
        <translation>FOUT</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="764"/>
        <source>Searching for</source>
        <extracomment>Call state</extracomment>
        <translation>Zoeken naar</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="767"/>
        <source>Aborted</source>
        <extracomment>Call state</extracomment>
        <translation>Afgebroken</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="770"/>
        <source>Communication established</source>
        <extracomment>Call state</extracomment>
        <translation>Communicatie tot stand gebracht</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="864"/>
        <source>Unknown</source>
        <translation>Onbekend</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="2231"/>
        <source>Account: </source>
        <translation>Account:</translation>
    </message>
</context>
<context>
    <name>CallModel</name>
    <message>
        <location filename="../src/callmodel.cpp" line="898"/>
        <source>Calls</source>
        <translation>Oproepen</translation>
    </message>
</context>
<context>
    <name>CallModelPrivate</name>
    <message>
        <location filename="../src/callmodel.cpp" line="533"/>
        <location filename="../src/callmodel.cpp" line="558"/>
        <source>Invalid account</source>
        <translation>Ongeldigen account</translation>
    </message>
</context>
<context>
    <name>CallPrivate</name>
    <message>
        <location filename="../src/call.cpp" line="1795"/>
        <source>Aborted</source>
        <translation>Afgebroken</translation>
    </message>
    <message>
        <location filename="../src/call.cpp" line="1816"/>
        <source>No account registered!</source>
        <translation>Genen account geregistreerd!</translation>
    </message>
</context>
<context>
    <name>CategorizedBookmarkModel</name>
    <message>
        <location filename="../src/categorizedbookmarkmodel.cpp" line="181"/>
        <source>Most popular</source>
        <extracomment>Most popular contacts</extracomment>
        <translation>Populairst</translation>
    </message>
    <message>
        <location filename="../src/categorizedbookmarkmodel.cpp" line="299"/>
        <source>Contacts</source>
        <translation>Contacten</translation>
    </message>
</context>
<context>
    <name>CategorizedContactModel</name>
    <message>
        <location filename="../src/categorizedcontactmodel.cpp" line="401"/>
        <source>Contacts</source>
        <translation>Contacten</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="129"/>
        <source>Empty</source>
        <translation>Leeg</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="135"/>
        <location filename="../src/private/sortproxies.cpp" line="153"/>
        <source>Unknown</source>
        <translation>Onbekend</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="141"/>
        <source>Never</source>
        <translation>Nooit</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="147"/>
        <source>Other</source>
        <translation>Anders</translation>
    </message>
</context>
<context>
    <name>CategorizedHistoryModel</name>
    <message>
        <location filename="../src/categorizedhistorymodel.cpp" line="412"/>
        <source>History</source>
        <translation>Geschiedenis</translation>
    </message>
</context>
<context>
    <name>ChainOfTrustModel</name>
    <message>
        <location filename="../src/chainoftrustmodel.cpp" line="173"/>
        <source>Chain of trust</source>
        <translation>Vertrouwensketen</translation>
    </message>
</context>
<context>
    <name>CollectionModel</name>
    <message>
        <location filename="../src/collectionmodel.cpp" line="279"/>
        <source>Name</source>
        <translation>Naam</translation>
    </message>
</context>
<context>
    <name>ContactSortingCategoryModel</name>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="49"/>
        <source>Name</source>
        <translation>Naam</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="50"/>
        <source>Organisation</source>
        <translation>Organisatie</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="51"/>
        <source>Recently used</source>
        <translation>Recent gebruikt</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="52"/>
        <source>Group</source>
        <translation>Groep</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="53"/>
        <source>Department</source>
        <translation>Afdeling</translation>
    </message>
</context>
<context>
    <name>HistorySortingCategoryModel</name>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="57"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="58"/>
        <source>Name</source>
        <translation>Naam</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="59"/>
        <source>Popularity</source>
        <translation>Populariteit</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="60"/>
        <source>Duration</source>
        <translation>Duur</translation>
    </message>
    <message>
        <location filename="../src/private/sortproxies.cpp" line="61"/>
        <source>Total time</source>
        <translation>Totale duur</translation>
    </message>
</context>
<context>
    <name>HistoryTimeCategoryModel</name>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="50"/>
        <source>Today</source>
        <translation>Vandaag</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="51"/>
        <source>Yesterday</source>
        <translation>Gisteren</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="58"/>
        <source>Two weeks ago</source>
        <translation>Twee weken geleden</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="59"/>
        <source>Three weeks ago</source>
        <translation>Drie weken geleden</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="57"/>
        <source>A week ago</source>
        <translation>Een week geleden</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="60"/>
        <source>A month ago</source>
        <translation>Een maand geleden</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="61"/>
        <source>Two months ago</source>
        <translation>Twee maand geleden</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="62"/>
        <source>Three months ago</source>
        <translation>Drie maand geleden</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="63"/>
        <source>Four months ago</source>
        <translation>Vier maand geleden</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="64"/>
        <source>Five months ago</source>
        <translation>Vijf maand geleden</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="65"/>
        <source>Six months ago</source>
        <translation>Zes maand geleden</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="66"/>
        <source>Seven months ago</source>
        <translation>Zeven maand geleden</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="67"/>
        <source>Eight months ago</source>
        <translation>Acht maand geleden</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="68"/>
        <source>Nine months ago</source>
        <translation>Negen maand geleden</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="69"/>
        <source>Ten months ago</source>
        <translation>Tien maand geleden</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="70"/>
        <source>Eleven months ago</source>
        <translation>Elf maand geleden</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="71"/>
        <source>Twelve months ago</source>
        <translation>Twaalf maand geleden</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="72"/>
        <source>A year ago</source>
        <translation>Een jaar geleden</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="73"/>
        <source>Very long time ago</source>
        <translation>Zeer lang geleden</translation>
    </message>
    <message>
        <location filename="../src/historytimecategorymodel.cpp" line="74"/>
        <source>Never</source>
        <translation>Nooit</translation>
    </message>
</context>
<context>
    <name>InstantMessagingModel</name>
    <message>
        <location filename="../src/media/textrecording.cpp" line="816"/>
        <source>Me</source>
        <translation>Ik</translation>
    </message>
</context>
<context>
    <name>MacroModel</name>
    <message>
        <location filename="../src/macromodel.cpp" line="157"/>
        <source>Macros</source>
        <translation>Macro’s</translation>
    </message>
    <message>
        <location filename="../src/macromodel.cpp" line="263"/>
        <source>New</source>
        <translation>Nieuw</translation>
    </message>
    <message>
        <location filename="../src/macromodel.cpp" line="264"/>
        <source>Other</source>
        <translation>Anders</translation>
    </message>
</context>
<context>
    <name>MacroModelPrivate</name>
    <message>
        <location filename="../src/macromodel.cpp" line="77"/>
        <source>Other</source>
        <translation>Anders</translation>
    </message>
</context>
<context>
    <name>NumberCategoryModel</name>
    <message>
        <location filename="../src/numbercategorymodel.cpp" line="56"/>
        <source>Uncategorized</source>
        <translation>Nie’ gecategoriseerd</translation>
    </message>
</context>
<context>
    <name>PersonModel</name>
    <message>
        <location filename="../src/personmodel.cpp" line="170"/>
        <source>Persons</source>
        <translation>Personen</translation>
    </message>
</context>
<context>
    <name>PhoneDirectoryModel</name>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="234"/>
        <source>This account does not support presence tracking</source>
        <translation>Dezen account ondersteund volgen van aanwezigheid nie</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="237"/>
        <source>No associated account</source>
        <translation>Gene g’associeerden account</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="305"/>
        <source>URI</source>
        <translation>URI</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="305"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="305"/>
        <source>Person</source>
        <translation>Persoon</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="305"/>
        <source>Account</source>
        <translation>Account</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="305"/>
        <source>State</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="305"/>
        <source>Call count</source>
        <translation>Oproepteller</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="305"/>
        <source>Week count</source>
        <translation>Weekteller</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="306"/>
        <source>Trimester count</source>
        <translation>Trimesterteller</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="306"/>
        <source>Have Called</source>
        <translation>Hebben gebeld</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="306"/>
        <source>Last used</source>
        <translation>Laatst gebruikt</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="306"/>
        <source>Name_count</source>
        <translation>Naam _teller</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="306"/>
        <source>Total (in seconds)</source>
        <translation>Totaal (in seconden)</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="306"/>
        <source>Popularity_index</source>
        <translation>Populariteits_index</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="307"/>
        <source>Bookmarked</source>
        <translation>Opgeslagen in bladwijzers</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="307"/>
        <source>Tracked</source>
        <translation>Gevolgd</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="307"/>
        <source>Present</source>
        <translation>Aanwezig</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="307"/>
        <source>Presence message</source>
        <translation>Aanwezigheidsbericht</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="307"/>
        <source>UID</source>
        <translation>UID</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="307"/>
        <source>Has certificate</source>
        <translation>Heefd certificaat</translation>
    </message>
    <message>
        <location filename="../src/phonedirectorymodel.cpp" line="307"/>
        <source>Registered name</source>
        <translation>Geregistreerde naam</translation>
    </message>
</context>
<context>
    <name>PresenceStatusModel</name>
    <message>
        <location filename="../src/presencestatusmodel.cpp" line="197"/>
        <source>Name</source>
        <translation>Naam</translation>
    </message>
    <message>
        <location filename="../src/presencestatusmodel.cpp" line="197"/>
        <source>Message</source>
        <translation>Bericht</translation>
    </message>
    <message>
        <location filename="../src/presencestatusmodel.cpp" line="197"/>
        <source>Color</source>
        <translation>Kleur</translation>
    </message>
    <message>
        <location filename="../src/presencestatusmodel.cpp" line="197"/>
        <source>Present</source>
        <translation>Aanwezig</translation>
    </message>
    <message>
        <location filename="../src/presencestatusmodel.cpp" line="197"/>
        <source>Default</source>
        <translation>Standaard</translation>
    </message>
    <message>
        <location filename="../src/presencestatusmodel.cpp" line="308"/>
        <location filename="../src/presencestatusmodel.cpp" line="361"/>
        <source>Custom</source>
        <translation>Aangepast</translation>
    </message>
    <message>
        <location filename="../src/presencestatusmodel.cpp" line="308"/>
        <location filename="../src/presencestatusmodel.cpp" line="310"/>
        <location filename="../src/presencestatusmodel.cpp" line="354"/>
        <location filename="../src/presencestatusmodel.cpp" line="361"/>
        <source>N/A</source>
        <translation>N.v.t.</translation>
    </message>
</context>
<context>
    <name>ProfileModel</name>
    <message>
        <location filename="../src/profilemodel.cpp" line="637"/>
        <source>Profiles</source>
        <translation>Profielen</translation>
    </message>
</context>
<context>
    <name>ProfileModelPrivate</name>
    <message>
        <location filename="../src/profilemodel.cpp" line="751"/>
        <source>New profile</source>
        <translation>Nieuw profiel</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/certificate.cpp" line="42"/>
        <source>Has a private key</source>
        <translation>Heefd ne persoonlijke sleutel</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="43"/>
        <source>Is not expired</source>
        <translation>Is nie’ vervallen</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="44"/>
        <source>Has strong signing</source>
        <translation>Heefd een sterke handtekening</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="45"/>
        <source>Is not self signed</source>
        <translation>Is nie’ zelfondertekend</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="46"/>
        <source>Have a matching key pair</source>
        <translation>Heefd een passend sleutelpaar</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="47"/>
        <source>Has the right private key file permissions</source>
        <translation>Heefd de juste bestandsmachtigingen voor de persoonlijke sleutel</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="48"/>
        <source>Has the right public key file permissions</source>
        <translation>Heefd de juste bestandsmachtigingen voor den openbare sleutel</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="49"/>
        <source>Has the right private key directory permissions</source>
        <translation>Heefd de juste machtigingen voor map met de persoonlijke sleutel</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="50"/>
        <source>Has the right public key directory permissions</source>
        <translation>Heefd de juste machtigingen voor map met den openbare sleutel</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="51"/>
        <source>Has the right private key directory location</source>
        <translation>Kend de juste locatie van de map met de persoonlijke sleutel</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="52"/>
        <source>Has the right public key directory location</source>
        <translation>Kend de juste locatie van de map met den openbare sleutel</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="53"/>
        <source>Has the right private key SELinux attributes</source>
        <translation>Heefd de juste SELinux-attributen voor de persoonlijke sleutel</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="54"/>
        <source>Has the right public key SELinux attributes</source>
        <translation>Heefd de juste SELinux-attributen voor den openbare sleutel</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="55"/>
        <source>The certificate file exist and is readable</source>
        <translation>Het certificaatsbestand bestaad en is leesbaar</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="56"/>
        <source>The file is a valid certificate</source>
        <translation>Het bestand is een geldig certificaat</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="57"/>
        <source>The certificate has a valid authority</source>
        <translation>Het certificaat heefd een geldige certificeringsinstantie</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="58"/>
        <source>The certificate has a known authority</source>
        <translation>Het certificaat heefd een gekende certificeringsinstantie</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="59"/>
        <source>The certificate is not revoked</source>
        <translation>Het certificaat is nie teruggetrokken</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="60"/>
        <source>The certificate authority match</source>
        <translation>De certificeringsinstantie komd overeen</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="61"/>
        <source>The certificate has the expected owner</source>
        <translation>Het certificaat heefd de verwachten eigenaar</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="62"/>
        <source>The certificate is within its active period</source>
        <translation>Het certificaat is in d’actieve periode</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="90"/>
        <source>Expiration date</source>
        <translation>Vervaldatum</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="91"/>
        <source>Activation date</source>
        <translation>Activeringsdatum</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="92"/>
        <source>Require a private key password</source>
        <translation>Vereist een paswoord voor de persoonlijke sleutel</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="93"/>
        <source>Public signature</source>
        <translation>Openbare handtekening</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="94"/>
        <source>Version</source>
        <translation>Versie</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="95"/>
        <source>Serial number</source>
        <translation>Serienummer</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="96"/>
        <source>Issuer</source>
        <translation>Uitgever</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="97"/>
        <source>Subject key algorithm</source>
        <translation>Onderwerpsleutelalgoritme</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="98"/>
        <source>Common name (CN)</source>
        <translation>Gewone naam (AN)</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="99"/>
        <source>Name (N)</source>
        <translation>Naam (N)</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="100"/>
        <source>Organization (O)</source>
        <translation>Organisatie (O)</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="101"/>
        <source>Signature algorithm</source>
        <translation>Handtekeningsalgoritme</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="102"/>
        <source>Md5 fingerprint</source>
        <translation>MD5-vingerafdruk</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="103"/>
        <source>Sha1 fingerprint</source>
        <translation>SHA1-vingerafdruk</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="104"/>
        <source>Public key ID</source>
        <translation>Openbare sleutel-ID</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="105"/>
        <source>Issuer domain name</source>
        <translation>Domeinnaam van uitgever</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="106"/>
        <source>Next expected update</source>
        <translation>Volgende verwachten update</translation>
    </message>
    <message>
        <location filename="../src/certificate.cpp" line="107"/>
        <source>Outgoing server</source>
        <translation>Uitgaande server</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="183"/>
        <source>Local certificate store</source>
        <translation>Lokale certificaatsoplag</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="267"/>
        <location filename="../src/localprofilecollection.cpp" line="220"/>
        <source>Default</source>
        <translation>Standaard</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="267"/>
        <source>Certificate not associated with a group</source>
        <translation>Certificaat nie’ g’associeerd met een groep</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="397"/>
        <source>A certificate</source>
        <translation>Een certificaat</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="397"/>
        <source>An organisation</source>
        <translation>Een organisatie</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="420"/>
        <source>Details</source>
        <translation>Details</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="420"/>
        <source>The content of the certificate</source>
        <translation>Den inhoud van het certificaat</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="421"/>
        <source>Checks</source>
        <translation>Controles</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="421"/>
        <source>Various security related information</source>
        <translation>Uiteenlopende beveiligingsgerelateerde informatie</translation>
    </message>
    <message>
        <location filename="../src/certificatemodel.cpp" line="610"/>
        <source>Header</source>
        <translation>Kop</translation>
    </message>
    <message>
        <location filename="../src/daemoncertificatecollection.cpp" line="148"/>
        <source>Daemon certificate store</source>
        <translation>Daemoncertificaatsopslag </translation>
    </message>
    <message>
        <location filename="../src/daemoncertificatecollection.cpp" line="152"/>
        <source>%1 banned list</source>
        <extracomment>The list of banned certificates for this account</extracomment>
        <translation>%1 lijst me’ verworpen certificaten</translation>
    </message>
    <message>
        <location filename="../src/daemoncertificatecollection.cpp" line="155"/>
        <source>%1 allowed list</source>
        <extracomment>The list of allowed certificates for this account</extracomment>
        <translation>%1 lijst met toegelaten certificaten</translation>
    </message>
    <message>
        <location filename="../src/daemoncertificatecollection.cpp" line="160"/>
        <location filename="../src/foldercertificatecollection.cpp" line="198"/>
        <source>Certificate</source>
        <translation>Certificaat</translation>
    </message>
    <message>
        <location filename="../src/extensions/presencecollectionextension.cpp" line="38"/>
        <source>Presence tracking</source>
        <translation>Aanwezigheid volgen</translation>
    </message>
    <message>
        <location filename="../src/extensions/securityevaluationextension.cpp" line="63"/>
        <source>Security evaluation</source>
        <translation>Beveiligingsevaluatie</translation>
    </message>
    <message>
        <location filename="../src/fallbackpersoncollection.cpp" line="194"/>
        <source>Contact</source>
        <translation>Contact</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="532"/>
        <source>Bookmark</source>
        <translation>Bladwijzer</translation>
    </message>
    <message>
        <location filename="../src/localhistorycollection.cpp" line="195"/>
        <source>Local history</source>
        <translation>Lokale geschiedenis</translation>
    </message>
    <message>
        <location filename="../src/localhistorycollection.cpp" line="200"/>
        <source>History</source>
        <translation>Geschiedenis</translation>
    </message>
    <message>
        <location filename="../src/localrecordingcollection.cpp" line="101"/>
        <source>Local recordings</source>
        <translation>Lokale opnames</translation>
    </message>
    <message>
        <location filename="../src/localrecordingcollection.cpp" line="106"/>
        <location filename="../src/localtextrecordingcollection.cpp" line="170"/>
        <source>Recording</source>
        <translation>Opname</translation>
    </message>
    <message>
        <location filename="../src/localringtonecollection.cpp" line="220"/>
        <source>Local ringtones</source>
        <translation>Lokale beltonen</translation>
    </message>
    <message>
        <location filename="../src/localringtonecollection.cpp" line="225"/>
        <source>Ringtone</source>
        <translation>Beltoon</translation>
    </message>
    <message>
        <location filename="../src/localtextrecordingcollection.cpp" line="165"/>
        <source>Local text recordings</source>
        <translation>Lokale tekstopnames</translation>
    </message>
    <message>
        <location filename="../src/numbercategory.cpp" line="72"/>
        <source>Phone number types</source>
        <translation>Telefoonnummertypes</translation>
    </message>
    <message>
        <location filename="../src/numbercategorymodel.cpp" line="186"/>
        <source>Other</source>
        <translation>Anders</translation>
    </message>
    <message>
        <location filename="../src/protocolmodel.cpp" line="50"/>
        <source>Ring Account</source>
        <translation>Ring-account</translation>
    </message>
    <message>
        <location filename="../src/protocolmodel.cpp" line="51"/>
        <source>SIP Account</source>
        <translation>SIP-account</translation>
    </message>
    <message>
        <location filename="../src/qtwrapper/callmanager_wrap.h" line="315"/>
        <source>Me</source>
        <translation>Ik</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="38"/>
        <source>TLS is disabled, the negotiation won&apos;t be encrypted. Your communication will be vulnerable to snooping</source>
        <translation>TLS is uitgeschakeld, d’onderhandeling zal nie worden versleuteld. Uw communicatie zal vatbaar zijn voor afluisteren</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="40"/>
        <source>Your certificate and authority don&apos;t match, if your certificate require an authority, it won&apos;t work</source>
        <translation>Uw certificaat en certificeringsinstantie komen nie overeen, als uw certificaat een certificeringsinstantie vereist zal het nie functioneren</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="41"/>
        <source>The outgoing server specified doesn&apos;t match the hostname or the one included in the certificate</source>
        <translation>De gespecificeerden uitgaande server komd nie overeen me’ d’hostnaam of me’ die opgegeven in het certificaat</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="42"/>
        <source>The &quot;verify incoming certificate&quot; option is disabled, this leave you vulnerable to man in the middle attack</source>
        <translation>D’optie ‘inkomende certificaten verifiëren’ is uitgeschakeld, dit maakt u vatbaar voor ne man-in-the-middle-aanval</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="43"/>
        <source>The &quot;verify answer certificate&quot; option is disabled, this leave you vulnerable to man in the middle attack</source>
        <translation>D’optie ‘antwoordcertificaten verifiëren’ is uitgeschakeld, dit maakt u vatbaar voor ne man-in-the-middle-aanval</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="44"/>
        <source>None of your certificate provide a private key, this is required. Please select a private key or use a certificate with one built-in</source>
        <translation>Geen van uw certificaten heefd ne persoonlijke sleutel, terwijl da’ dit vereist is. Kiesd ne persoonlijke sleutel of gebruikt een certificaat met nen ingebouwde sleutel</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="47"/>
        <source>No certificate authority is provided, it won&apos;t be possible to validate if the answer certificates are valid. Some account may also not work.</source>
        <translation>Geen certificeringsinstantie opgegeven, het zal nie mogelijk zijn voor d’inkomende certificaten te valideren. Sommige accounts werken mogelijks ook nie.</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="46"/>
        <source>No certificate has been provided. This is, for now, unsupported by Ring</source>
        <translation>Geen certificaat opgegeven. Dit word, vooralsnog, nie’ door Ring ondersteund</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="37"/>
        <source>Your media streams are not encrypted, please enable SDES</source>
        <translation>Uw mediastreams zijn nie’ versleuteld, schakeld SDES in</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="50"/>
        <source>Your certificate is expired, please contact your system administrator.</source>
        <translation>Uw certificaat is vervallen, neemd contact op met uwe systeembeheerder.</translation>
    </message>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="51"/>
        <source>Your certificate is self signed. This break the chain of trust.</source>
        <translation>Uw certificaat is zelfondertekend. Dit verbreekt de vertrouwensketen.</translation>
    </message>
    <message>
        <location filename="../src/tlsmethodmodel.cpp" line="64"/>
        <source>Default</source>
        <comment>Default TLS protocol version</comment>
        <translation>Standaard</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="517"/>
        <location filename="../src/useractionmodel.cpp" line="759"/>
        <source>Accept</source>
        <translation>Aanvaarden</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="60"/>
        <location filename="../src/useractionmodel.cpp" line="518"/>
        <location filename="../src/useractionmodel.cpp" line="771"/>
        <source>Hold</source>
        <translation>In wacht zetten</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="62"/>
        <source>Talking</source>
        <translation>Praten</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="64"/>
        <source>ERROR</source>
        <translation>FOUT</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="66"/>
        <source>Incoming</source>
        <translation>Inkomend</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="68"/>
        <source>Calling</source>
        <translation>Bellen</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="70"/>
        <source>Connecting</source>
        <translation>Verbinden</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="72"/>
        <source>Searching</source>
        <translation>Zoeken</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="74"/>
        <source>Inactive</source>
        <translation>Inactief</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="76"/>
        <location filename="../src/api/call.h" line="82"/>
        <source>Finished</source>
        <translation>Voltooid</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="78"/>
        <source>Timeout</source>
        <translation>Time-out</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="80"/>
        <source>Peer busy</source>
        <translation>Contact is bezig</translation>
    </message>
    <message>
        <location filename="../src/api/call.h" line="84"/>
        <source>Communication established</source>
        <translation>Communicatie tot stand gebracht</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="519"/>
        <source>Mute audio</source>
        <translation>Geluid dempen</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="520"/>
        <source>Mute video</source>
        <translation>Video uitschakelen</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="521"/>
        <source>Server transfer</source>
        <translation>Serveropdracht</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="522"/>
        <source>Record</source>
        <translation>Opnemen</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="523"/>
        <location filename="../src/useractionmodel.cpp" line="789"/>
        <source>Hangup</source>
        <translation>Ophangen</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="524"/>
        <source>Join</source>
        <translation>Deelnemen</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="525"/>
        <source>Add new</source>
        <translation>Nieuw toevoegen</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="526"/>
        <source>Toggle video</source>
        <translation>Video in-/uitschakelen</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="527"/>
        <source>Add a contact</source>
        <translation>Een contact toevoegen</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="528"/>
        <source>Add to existing contact</source>
        <translation>Toevoegen aan bestaand contact</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="529"/>
        <source>Delete contact</source>
        <translation>Contact verwijderen</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="530"/>
        <source>Email contact</source>
        <translation>Contact e-mailen</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="531"/>
        <source>Copy contact</source>
        <translation>Contact kopiëren</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="534"/>
        <source>Add phone number</source>
        <translation>Telefoonnummer toevoegen</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="535"/>
        <source>Call again</source>
        <translation>Terug bellen</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="536"/>
        <source>Edit contact details</source>
        <translation>Contactgegevens bewerken</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="537"/>
        <source>Remove from history</source>
        <translation>Verwijderen uit geschiedenis</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="786"/>
        <source>Remove</source>
        <translation>Verwijderen</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="756"/>
        <source>Call</source>
        <translation>Bellen</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="533"/>
        <source>Open chat</source>
        <translation>Babbel openen</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="768"/>
        <source>Unhold</source>
        <translation>Uit wacht halen</translation>
    </message>
    <message>
        <location filename="../src/useractionmodel.cpp" line="779"/>
        <source>Cancel</source>
        <translation>Annuleren</translation>
    </message>
    <message>
        <location filename="../src/localprofilecollection.cpp" line="147"/>
        <source>Local profiles</source>
        <translation>Lokale profielen</translation>
    </message>
    <message>
        <location filename="../src/localprofilecollection.cpp" line="152"/>
        <source>Profile Collection</source>
        <translation>Profielverzameling</translation>
    </message>
    <message>
        <location filename="../src/peerprofilecollection.cpp" line="141"/>
        <source>Peer profiles</source>
        <translation>Peerprofielen</translation>
    </message>
    <message>
        <location filename="../src/peerprofilecollection.cpp" line="146"/>
        <source>Peers Profiles Collection</source>
        <translation>Peerprofielverzameling</translation>
    </message>
    <message>
        <location filename="../src/conversationmodel.cpp" line="1265"/>
        <location filename="../src/conversationmodel.cpp" line="1649"/>
        <location filename="../src/conversationmodel.cpp" line="1909"/>
        <source>Invitation received</source>
        <translation>Uitnodiging ontvangen</translation>
    </message>
    <message>
        <location filename="../src/conversationmodel.cpp" line="1266"/>
        <source>Contact added</source>
        <translation>Contact toegevoegd</translation>
    </message>
    <message>
        <location filename="../src/conversationmodel.cpp" line="1308"/>
        <location filename="../src/conversationmodel.cpp" line="1315"/>
        <source>Invitation accepted</source>
        <translation>Uitnodiging aanvaard</translation>
    </message>
    <message>
        <location filename="../src/conversationmodel.cpp" line="1524"/>
        <source>ð Outgoing call</source>
        <translation>ð Uitgaanden oproep</translation>
    </message>
    <message>
        <location filename="../src/conversationmodel.cpp" line="1526"/>
        <source>ð Incoming call</source>
        <translation>ð Inkomenden oproep</translation>
    </message>
    <message>
        <location filename="../src/conversationmodel.cpp" line="1539"/>
        <source>ð Outgoing call - </source>
        <translation>ð Uitgaanden oproep - </translation>
    </message>
    <message>
        <location filename="../src/conversationmodel.cpp" line="1542"/>
        <source>ð Incoming call - </source>
        <translation>ð Inkomenden oproep - </translation>
    </message>
    <message>
        <location filename="../src/conversationmodel.cpp" line="1546"/>
        <source>ð½ Missed outgoing call</source>
        <translation>ð½ Gemisten uitgaanden oproep</translation>
    </message>
    <message>
        <location filename="../src/conversationmodel.cpp" line="1548"/>
        <source>ð½ Missed incoming call</source>
        <translation>ð½ Gemisten inkomenden oproep</translation>
    </message>
</context>
<context>
    <name>RingDeviceModel</name>
    <message>
        <location filename="../src/ringdevicemodel.cpp" line="110"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../src/ringdevicemodel.cpp" line="112"/>
        <source>Name</source>
        <translation>Naam</translation>
    </message>
</context>
<context>
    <name>SecurityEvaluationModelPrivate</name>
    <message>
        <location filename="../src/securityevaluationmodel.cpp" line="683"/>
        <source>Authority</source>
        <translation>Autoriteit</translation>
    </message>
</context>
<context>
    <name>TlsMethodModel</name>
    <message>
        <location filename="../src/tlsmethodmodel.cpp" line="101"/>
        <source>Automatic</source>
        <translation>Automatisch</translation>
    </message>
</context>
<context>
    <name>Video::SourceModel</name>
    <message>
        <location filename="../src/video/sourcemodel.cpp" line="95"/>
        <source>NONE</source>
        <translation>GEEN</translation>
    </message>
    <message>
        <location filename="../src/video/sourcemodel.cpp" line="101"/>
        <source>SCREEN</source>
        <translation>SCHERM</translation>
    </message>
    <message>
        <location filename="../src/video/sourcemodel.cpp" line="107"/>
        <source>FILE</source>
        <translation>BESTAND</translation>
    </message>
</context>
<context>
    <name>lrc::ContactModelPimpl</name>
    <message>
        <location filename="../src/contactmodel.cpp" line="393"/>
        <source>Searchingâ¦</source>
        <translation>Zoeken</translation>
    </message>
    <message>
        <location filename="../src/contactmodel.cpp" line="747"/>
        <source>Invalid ID</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../src/contactmodel.cpp" line="750"/>
        <source>Not found</source>
        <translation>Niet gevonden</translation>
    </message>
    <message>
        <location filename="../src/contactmodel.cpp" line="753"/>
        <source>Couldn&apos;t lookupâ¦</source>
        <translation>Kan niet opzoeken</translation>
    </message>
</context>
<context>
    <name>lrc::api::ContactModel</name>
    <message>
        <location filename="../src/contactmodel.cpp" line="364"/>
        <source>Bad URI scheme</source>
        <translation>Slecht URI schema</translation>
    </message>
</context>
<context>
    <name>media::RecordingModel</name>
    <message>
        <location filename="../src/media/recordingmodel.cpp" line="189"/>
        <source>Recordings</source>
        <translation>Opnames</translation>
    </message>
    <message>
        <location filename="../src/media/recordingmodel.cpp" line="259"/>
        <source>Text messages</source>
        <translation>Tekstberichten</translation>
    </message>
    <message>
        <location filename="../src/media/recordingmodel.cpp" line="264"/>
        <source>Audio/Video</source>
        <translation>Audio/video</translation>
    </message>
</context>
</TS>